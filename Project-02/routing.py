# function for figuring out routing in a chain
def route_Chain(selfNodeID, destinationNodeID):
    # if we've already reached destination node return -1
    if selfNodeID == destinationNodeID:
        return -1, 0
    else:
        # otherwise return the next node in the chain(L to R routing)
        #if selfNodeID > destinationNodeID:
        #    return selfNodeID - 1
        #else
        #    return selfNodeID + 1

        return selfNodeID + (2*int(destinationNodeID>selfNodeID)-1), 0 # Add +1 or -1


def min_dist(id1, id2, N):
    if(abs(id1-id2) <= N//2):
        return id1-id2
    else:
        if(id1-id2 <= 0):
            return  N+id1-id2
        else:
            return id1-id2-N

# function for figuring out routing in a ring
def route_Ring(selfNodeID, destinationNodeID, vc, N):  #initialize vc to 0 initially when selfnode==init node
    next_vc = vc
    # if we've already reached destination node return -1
    if selfNodeID == destinationNodeID:
        if(destinationNodeID == N//2 + 1):
            next_vc = 1
        return -1, next_vc

    elif (min_dist(selfNodeID,destinationNodeID, N) > 0):
        #traverse anticlockwise
        if(selfNodeID - 1 == N//2):  #crosses dateline
            print("change next_vc to 1")
            next_vc = 1

        if(selfNodeID==0):
            return N-1, next_vc
        else:
            return selfNodeID - 1, next_vc
    else:
        # traverse clockwise
        if(selfNodeID + 1 == N//2 + 1): #crosses dateline
            print("change next_vc to 1")
            next_vc = 1

        if(selfNodeID==N-1):
            return 0, next_vc
        else:
            return selfNodeID + 1, next_vc

# function for figuring out routing in a mesh
def route_Mesh(selfNodeID, destinationNodeID, n):
    ## already the minima hs been subtracted
    # if we've already reached destination node return -1
    self_i = selfNodeID // n
    self_j = selfNodeID % n
    dest_i = destinationNodeID // n
    dest_j = destinationNodeID % n

    if self_i == dest_i and self_j == dest_j:
        return -1, 0
    else:
        # otherwise return the next node in the chain(L to R routing)
        if abs(dest_j-self_j) > abs(dest_i - self_i):
            self_j = self_j + (2*int(dest_j>self_j)-1)
            return n*self_i + self_j, 0 # Add +1 or -1
        else:
            self_i = self_i + (2*int(dest_i>self_i)-1)
            return n*self_i + self_j, 0 # Add +1 or -1
            #return selfNodeID + (2*int(dest_i>self_i)-1), 0 # Add +1 or -1


def direction_ft(id1, id2, N):
    # direction = 1 means clockwise
    dir = 0
    #print(min_dist(id1, id2, N))
    if (min_dist(id1, id2, N)<0):
        dir = 1
    else:
        dir = 0
    #print(dir)
    return dir

# function for figuring out routing in a folded torus
def route_FT(selfNodeID, destinationNodeID, n, m, vc):
    # if we've already reached destination node return -1
    self_i = selfNodeID // n
    self_j = selfNodeID % n
    dest_i = destinationNodeID // n
    dest_j = destinationNodeID % n
    #print(self_i, self_j)
    if selfNodeID == destinationNodeID:
        #print(self_i, self_j)
        return -1, 0

    else:
        # X distance
        #print(0,self_i, self_j)
        next_vc = 0
        if (dest_j != self_j):  #traverse columns
            direction = direction_ft(self_j, dest_j, m)
            self_j = self_j + (2*int(direction)-1)
            self_j = self_j%m
            #print(self_i, self_j)
            return n*self_i + self_j, next_vc # Add +1 or -1
        else:  #traverse ring
            direction = direction_ft(self_i, dest_i, n)
            self_i = self_i + (2*int(direction)-1)
            self_i = self_i%n

            if(direction == 0 and self_i - 1 == n//2):  #crosses dateline
                print("change next_vc to 1")
                next_vc = 1

            elif(direction == 1 and self_i == n//2 + 1): #crosses dateline
                print("change next_vc to 1")
                next_vc = 1

            #print(self_i, self_j)
            return n*self_i + self_j, next_vc



##
##
##def direction_ft(id1, id2, N):
##    # direction = 1 means clockwise
##    dir = 0
##    if (id1-id2)%2 == 0:
##        dir = (id2 < id1)
##    else:
##        dN1 = N - id1 - 1
##        dN2 = N - id2 - 1
##        if dN1 + dN2 > id1 + id2:
##            dir = 1
##        else:
##            dir = 0
##    return dir
##
### function for figuring out routing in a folded torus
##def route_FT(selfNodeID, destinationNodeID, n, m, vc):
##    # if we've already reached destination node return -1
##    self_i = selfNodeID // n
##    self_j = selfNodeID % n
##    dest_i = destinationNodeID // n
##    dest_j = destinationNodeID % n
##    if selfNodeID == destinationNodeID:
##        return -1, 0
##
##    else:
##        # X distance
##        #print(0,self_i, self_j)
##        next_vc = 0
##        if (dest_j != self_j):
##            direction = direction_ft(self_j, dest_j, m)
##            if (self_j == 0) or (self_j == m-1):
##            #if (self_j == 0 and direction==1) or (self_j == m-1 and direction==0):
##                # direction = 1 means clockwise
##                #print((direction), self_j, dest_j)
##                self_j = self_j + (2*int(direction)-1)
##            else:
##                self_j = self_j + 2*(2*int(direction)-1)
##            self_j = self_j%m
##            return n*self_i + self_j, next_vc # Add +1 or -1
##        else:
##            direction = direction_ft(self_i, dest_i, n)
##            #if (self_i == 0 and direction==1) or (self_i == n-1 and direction==0):
##            if (self_i == 0) or (self_i == n-1):
##                self_i = self_i + (2*int(direction)-1)
##            else:
##                self_i = self_i + 2*(2*int(direction)-1)
##            self_i = self_i%n
##
##            if(self_i == n//2): #crosses dateline
##                print("change next_vc to 1")
##                next_vc = 1
##
##
##        return n*self_i + self_j, next_vc
##


        # shortest date line?
        # direction = 1
        # if self_i > dest_i and abs(self_i-dest_i) > n//2:
        #   direction = 1
        # elif self_i > dest_i and abs(self_i-dest_i) < n//2:
        #   if self_i == n-1:
        #     direction = 1
        #   else:
        #     direction = -1
        # elif self_i < dest_i and abs(self_i-dest_i) > n//2:
        #   direction = -1
        # else:
        #   if self_i == 0:
        #     direction = -1
        #   else:
        #     direction = 1

        # #print(direction)
        # if direction == 1:
        #   if self_i == 0:
        #         self_i = self_i +1
        #   elif self_i == n-1:
        #         self_i = self_i -1
        #   else:
        #         self_i = self_i+2
        #   self_i = self_i %n
        # else:
        #   if self_i == 1:
        #         self_i = self_i -1
        #   elif self_i == n-2:
        #         self_i = self_i +1
        #   else:
        #         self_i = self_i-2
        #   self_i = self_i %n

        # return n*self_i + self_j, 0 # Add +1 or -1

        #if abs(self_i-self_j) > n//2:
        #    if (self_i == 0 or self_i == n-1) and (abs(dest_i-self_i) < n//2):
        #        self_i = self_i - (2*int(dest_i>self_i)-1)
        #    else:
        #        self_i = self_i - (2*int(dest_i>self_i)-1)*2
        #    self_i = self_i%n
        #    return n*self_i + self_j, 0 # Add +1 or -1
        #else:
        #    if (self_i == 0 or self_i == n-1) and (abs(dest_i-self_i) < n//2):
        #        self_i = self_i + (2*int(dest_i>self_i)-1)
        #    else:
        #        self_i = self_i + (2*int(dest_i>self_i)-1)*2
        #    self_i = self_i%n
        #    return n*self_i + self_j, 0 # Add +1 or -1

# function for finding routing in a hyercube of dimension 3
def route_Hypercube(selfNodeID, destinationNodeID):
    # if we've already reached destination node return -1
    if selfNodeID == destinationNodeID:
        return -1

    else:
        # find bitstring representations
        d_bit = format(destinationNodeID, "03b")
        s_bit = format(selfNodeID, "03b")

        next_node = -1
        # find next node using MSB to LSB routing
        if d_bit[0] != s_bit[0]:
            next_node = int(format(not eval(s_bit[0]), "01b") + s_bit[1:], 2)
        elif d_bit[1] != s_bit[1]:
            next_node = int(s_bit[0] + format(not eval(s_bit[1]), "01b") + s_bit[2], 2)
        else:
            next_node = int(s_bit[0:2] + format(not eval(s_bit[2]), "01b"), 2)

        # we assign the virtual channel by where it should go next
        # this way we actually need only 3 vc's per node (in just a hypercube)
        # virtual channel follows the same algorithm as node, so just recursively call
        # this may not be the most hardware efficient way though
        # for hardware just write the corresponding code here
        #vc = route_Hypercube(next_node, destinationNodeID)

        # return next_node and virtual channel allocated
        return next_node, destinationNodeID


import math

# function for figuring out routing in a butterfly
def route_Butterfly(selfNodeID, destinationNodeID, N):
    # if we've already reached destination node return -1
    #print(selfNodeID, destinationNodeID, N)
    if selfNodeID == destinationNodeID:
        return -1, 0

    flip = 0
    if selfNodeID > destinationNodeID:
      flip = 1
      selfNodeID, destinationNodeID = destinationNodeID, selfNodeID
    #print(selfNodeID, destinationNodeID)

    s_bit = format(selfNodeID, "0"+str(int(math.log2(N)))+"b")



    #d_bit = format(destinationNodeID, "0"+str(math.log2(N))+"b")
    did = (bin(destinationNodeID - N - int(int(math.log2(N))*(N//2)))[2:].zfill(int(math.log2(N))))
    #print(did)
    #sid = bin(switchID)[2:].zfill(int(math.log2(N//2)))
    nodes = []

    prev_sbit = int(s_bit, 2)

    add_0 = 0
    add_1 = 0
    switchID_i = 0

    switch_list = []
    vc_list = []
    for i in range(int(math.log2(N))+1):
        if i == 0:
            #print('abc')
            prev_sbit = prev_sbit//2
            switchID_i = N+prev_sbit
            switch_list.append(switchID_i)
            vc_list.append(0)
        else:
            if i<=int(math.log2(N)-1):
                #print('hi', i, (N//2**(i)))
                #add_0 = min(prev_sbit, (prev_sbit + int(N//(2**(i+1))))%(N//(2**(i)))+ (prev_sbit//(N//(2**(i))))*(N//(2**(i+1))) )
                #add_1 = max(prev_sbit, (prev_sbit + int((N//(2**(i+1)))))%(N//(2**(i))) +(prev_sbit//(N//(2**(i))))*(N//(2**(i+1))) )
                #add_0 = min(prev_sbit, (prev_sbit + int(math.log2(N//(2**(i+1)))))%(N//2))
                #add_1 = max(prev_sbit, (prev_sbit + int(math.log2(N//(2**(i+1)))))%(N//2))
                #print(i, prev_sbit, did[i-1], add_0, add_1, N+i*N//2, N//(2**(i)), int(N//(2**(i+1)))%(N//(2**(i))), (prev_sbit + int(N//(2**(i+1))))%(N//(2**(i))))
                #print(i, prev_sbit, add_0, add_1, N, N//(2**i))

                a = format(prev_sbit, "0"+str(int(math.log2(N//2)))+"b")
                #print('a ', a)
                b = int(a[0:i-1] + format(not eval(a[i-1]), "01b") + a[i:], 2)
                a = int(a,2)

                #print(a,b)

                add_0 = min(a, b)
                add_1 = max(a, b)
            else:
                add_0 = 2*prev_sbit
                add_1 = 2*prev_sbit+1


            if did[i-1] == '0':
              #print('hi')
              prev_sbit = add_0
              switchID_i = (i)*N//2+add_0+N
              #switchID_i = i*N//2+prev_sbit//2+N
            else:
              #prev_sbit = (prev_sbit + int(math.log2(N//(2**(i+1)))))%(N//2)
              #switchID_i = i*N//2+prev_sbit//2+N
              prev_sbit = add_1
              switchID_i = (i)*N//2+add_1+N
            switch_list.append(switchID_i)
            vc_list.append(0)

        #print(switchID_i)
    if flip == 0:
        return switch_list, vc_list

    else:
        #print(switch_list)
        switch_list.pop(-1)
        switch_list.reverse()
        switch_list.append(selfNodeID)
        return switch_list, vc_list



    #elif(selfNodeID<N): #input layer
    #    return N+(selfNodeID//2), 0
    #elif(selfNodeID<N+math.log2(N)*(N//2)): #switch layer
    #    switchID = selfNodeID - N
    #    tier = switchID // (N//2)
    #    b = int(did[int(tier)])
    #    print(did,tier, b)
    #    if(tier==math.log2(N)-1):  #last switch layer
    #        return N + int(math.log2(N)*(N//2)) + 2*(switchID%(N//2)) + b, 0
    #    else: #tier<math.log2(N)-1)
    #        if(math.log2(N)-tier-3>0):
    #            return N + (tier+1)*(N//2) + b*(N//(2**(tier+2))) + int(sid[int(math.log2(N//2)-2-tier):int(math.log2(N//2))], 2), 0
    #        else:
    #            return N + (tier+1)*(N//2) + b*(N//(2**(tier+2))) + int(sid[int(math.log2(N//2)-2)]), 0


                #print(math.log2(N)-tier-3,  N, (tier+1)*(N//2), b*(N//(2**(tier+2))), sid,int(math.log2(N//2)-2-tier),int(math.log2(N//2)),int(sid[int(math.log2(N//2)-2-tier):int(math.log2(N//2))]))
                #print(math.log2(N)-tier-3,  N, (tier+1)*(N//2), b*(N//(2**(tier+2))), sid,int(sid[int(math.log2(N//2)-1)]))
            #print( N ,int(math.log2(N)*(N//2)), 2*(switchID%(N//2)), b)

#print(route_Hypercube(5,7))

def test(start, end):
    a = start
    print(a)
    while a!= end:
      #a, vc = route_Chain(a, end)
      #a, vc = route_Mesh(a, end, 5)
      a, vc = route_FT(a, end, 4, 4, 0)
      #a, vc = route_Ring(a, end, 0, 4)
      #a, vc = route_Hypercube(a, end)
      print(a, vc)
    #a, vc = route_Butterfly(a, end, 16)

#test(1,115)

#test(9,56)
#test(56,9)
#test(6,21)
#print('')
#test(5,23)
#print('')
#test(4,24)
#print('')
#test(3,27)
#print('')

#test(1,53)
#print('')
#test(15,57)
#print('')
#test(4,58)
#print('')
#test(13,62)
#print('')

#test(2,1)
#print('')
#test(1,3)
#print('')
#test(0,2)
#print('')
#test(3,2)
#print('')
#test(0,3)
#test(12,1)
#print('')
#test(1,13)
#print('')
#test(9,4)
#print('')
#test(4,15)
#print('')
#test(0,13)

# 000 to 011
# 000 - 001 (1st VC)
# 001 - 011 (3rd VC)

# 000 to 111, 7
# 001 to 111, 7

# 000, 1
# 001, 3
# 011, _

# 000, 3
# 001, 3
# 011, 3

# 000, _
# 001, 1
# 011, 3

## Input node1, node2 (23, 74)

## Check tile1 = tileID(node1), tile2 = tileID(node2)
        #for i in net.tiles
        #    if node1 in i.nodes
        #      tile1 = i
        #      break

#if tile1 != tile2
## route(node1, tile1_Head), route(tile2_Head, node2)

#### route(tile1_Head, tile2_Head)















