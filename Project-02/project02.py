# NOC project part 1

# Import log for butterfly network
from math import log
from routing import *

# Class to define a single node in the NOC.
# Attributes include:
# 1. head : the head of the node, which is itself
# 2. name : the name of the node
# 3. id : global id number of the node
# 4. links : the links of the node as a list of nodes it connects to
class node:
    def __init__(self, ID = 0, name = "", tile_ID = -1):
        self.head = self
        self.id = ID
        self.tile_ID = tile_ID
        self.name = name
        self.links = []

# Class to define a network. This may be used as a tile in the complete network
# or even as the entire network. Extendable to multiple layered networks.
# Attributes include:
# 1. n : size of1st dimension of the network
# 2. m : size of 2nd dimension of the network
# 3. node_start_id : the id of (lexicographically) first node in the network
# 4. name : name of the network
# 5. net_type : type of network:Eg. ring
# 6. nodes : list of nodes in the network
# 7. num_nodes : total number of nodes in the network
# 8. head : defines the head of the network, which will connect to other networks when used as a tile in a higher order network.

class Net:
    def __init__(self, ID = 0, node_start_id = 0, net_type = "C", n = 1, m = 1, tile_start_ID = 0):
        self.n = n
        self.m = m
        self.id = ID
        self.node_start_id = node_start_id
        self.tile_start_ID = tile_start_ID
        self.name = net_type + str(ID)
        self.net_type = net_type
        self.nodes = []
        self.num_nodes = 0
        # Do not assign head yet. Only once we create the links.
        self.head = None

    # Function to add a tile(of class Net) to the higher order network(of class Net)
    def add_tile_to_net(self, tile):
        self.nodes.append(tile)

    # Calculate number of nodes for each net.
    def calc_num_nodes(self):
        if self.net_type == "C":
            self.num_nodes  = self.n

        elif self.net_type == "R":
            self.num_nodes  = self.n

        elif self.net_type == "M":
            self.num_nodes = self.n * self.m;

        elif self.net_type == "F":
            self.num_nodes = self.n * self.m;

        elif self.net_type == "H":
            self.num_nodes = 2**3

        elif self.net_type == "B":
            # For the butterfly network we add only the input output nodes for now.
            # The internal nodes, or switches, will be added while creating the network.
            self.num_nodes = 2*self.n

    # Function to add nodes to a tile in a network based on the type of network.
    # To be used explicitly for a tile, which is a network of 2nd order
    def add_nodes_to_tile(self):
        if self.net_type == "C":
#            self.num_nodes  = self.n
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "R":
#            self.num_nodes  = self.n
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "M":
#            self.num_nodes = self.n * self.m;
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i%self.n) + str(i//self.n), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "F":
#            self.num_nodes = self.n * self.m;
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i%self.n) + str(i//self.n), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "H":
#            self.num_nodes = 2**3
            for i in range(2**3):
                temp = node(name = self.name + "_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "B":
            # For the butterfly network we add only the input output nodes for now.
            # The internal nodes, or switches, will be added while creating the network.
            k = int(log(self.n, 2))
            num_switches_per_layer = 2 ** (k - 1)
#            self.num_nodes = (self.n//2)*(k + 4)
            start_layers = self.n
            end_layers = start_layers + k * num_switches_per_layer

            for i in range(2*self.n):
                if i < start_layers:
                    temp = node(name = self.name + "_inp_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                    self.nodes.append(temp.head)
#                elif i < end_layers:
#                    index = i - start_layers
#                    temp = node(name = self.name + "_switch" + str(index//num_switches_per_layer) + str(index%num_switches_per_layer), ID = self.node_start_id + i, tile_ID = tile_id)
#                    self.nodes.append(temp.head)
                else:
                    temp = node(name = self.name + "_out_" + str(i - start_layers), ID = self.node_start_id + end_layers - self.n +i, tile_ID = tile_id)
                    #temp = node(name = self.name + "_out_" + str(i - start_layers), ID = self.node_start_id + i, tile_ID = tile_id)
                    self.nodes.append(temp.head)

    # Wrapper function to call the appropriate function to create the links between the nodes in the network
    def create_net(self):
        if self.net_type == "C":
            self.create_chain()

        elif self.net_type == "R":
            self.create_ring()

        elif self.net_type == "M":
            self.create_mesh()

        elif self.net_type == "F":
            self.create_foldedTorus()

        elif self.net_type == "H":
            self.create_hypercube()

        elif self.net_type == "B":
            self.create_butterfly()

    # Function to create links between nodes in a chain.
    def create_chain(self):
        for i in range(self.n - 1):
            # We only need to create a link between adjacent nodes, in only one dimension.
            self.nodes[i].head.links.append(self.nodes[i+1].head)
            self.nodes[i+1].head.links.append(self.nodes[i].head)

        # Assign the middle node as the head of the network.
        self.head = self.nodes[self.n//2].head

    # Function to create links between nodes in a ring
    def create_ring(self):
        for i in range(self.n):
            # We need to make all the links as a chain, and additionally the link between the last first node. Taking mod makes this easier to do.
            self.nodes[i].head.links.append(self.nodes[(i+1)%(self.n)].head)
            self.nodes[(i+1)%self.n].head.links.append(self.nodes[i].head)

        # The ring is symmetric, hence it doesn't matter which node we choose as head
        self.head = self.nodes[0].head

    # Function to create links between nodes in a mesh
    def create_mesh(self):
        for i in range(self.n):
            for j in range(self.m):
                # Create links in the first dimension. Is the same as a chain.
                if i < self.n - 1:
                    self.nodes[i + j * self.n].head.links.append(self.nodes[i + j * self.n + 1].head)
                    self.nodes[i + j * self.n + 1].head.links.append(self.nodes[i + j * self.n].head)
                # Create links in second dimension. It's like a chain in the other dimension also.
                if j < self.m - 1:
                    self.nodes[i + j * self.n].head.links.append(self.nodes[i + (j + 1) * self.n].head)
                    self.nodes[i + (j + 1) * self.n].head.links.append(self.nodes[i + j * self.n].head)

        # Choose the middle node of the mesh as the head.
        self.head = self.nodes[self.n//2 + (self.m//2) * self.n].head

    # Function to create links between nodes in a folded torus.
    def create_foldedTorus(self):
        for i in range(self.n):
            for j in range(self.m):
                # Need to create links between alternate nodes in each dimension.
                # But also need to take care of the boundary nodes, where they connect with the adjacent node too.
                if self.n > 1:
                    # First node.
                    if i == 0:
                        self.nodes[i + j * self.n].head.links.append(self.nodes[i + j * self.n + 1].head)
                        self.nodes[i + j * self.n + 1].head.links.append(self.nodes[i + j * self.n].head)

                    # Every node but first and last
                    if i < self.n - 2:
                        self.nodes[i + j * self.n].head.links.append(self.nodes[i + j * self.n + 2].head)
                        self.nodes[i + j * self.n + 2].head.links.append(self.nodes[i + j * self.n].head)
                    # Last node
                    elif i == self.n - 1:
                        self.nodes[i + j * self.n].head.links.append(self.nodes[i + j * self.n - 1].head)
                        self.nodes[i + j * self.n - 1].head.links.append(self.nodes[i + j * self.n].head)

                # Same as above, in second dimension.
                if self.m >= 1:
                    if j == 0:
                        self.nodes[i + j * self.n].head.links.append(self.nodes[i + (j + 1) * self.n].head)
                        self.nodes[i + (j + 1) * self.n].head.links.append(self.nodes[i + j * self.n].head)

                    if j < self.m - 2:
                        self.nodes[i + j * self.n].head.links.append(self.nodes[i + (j + 2) * self.n].head)
                        self.nodes[i + (j + 2) * self.n].head.links.append(self.nodes[i + j * self.n].head)
                    elif j == self.m - 1:
                        self.nodes[i + j * self.n].head.links.append(self.nodes[i + (j - 1) * self.n].head)
                        self.nodes[i + (j - 1) * self.n].head.links.append(self.nodes[i + j * self.n].head)

        # Again choose the middle node of the network as the head
        self.head = self.nodes[self.n//2 + (self.m//2) * self.n].head
        #self.head = self.nodes[self.n//2 + (self.m//2) * self.n]

    # Function to create links between nodes in a hypercube(of dimension 3)
    def create_hypercube(self):
        # Need to create a simple cube connection. Adjacent nodes are of one Hamming distance away.
        for i in range(2**3):
            bitstring = format(i,"03b")
            n1 = format(not eval(bitstring[0]), "01b") + bitstring[1:]
            n2 = bitstring[0] + format(not eval(bitstring[1]), "01b") + bitstring[2]
            n3 = bitstring[0:2] + format(not eval(bitstring[2]), "01b")
            self.nodes[i].head.links.append(self.nodes[int(n1, 2)].head)
            self.nodes[i].head.links.append(self.nodes[int(n2, 2)].head)
            self.nodes[i].head.links.append(self.nodes[int(n3, 2)].head)

        # The hypercube is also a symmetric structure, so we can choose any node as the head.
        self.head = self.nodes[0].head

    # Function to create links between nodes in a butterfly network
    def create_butterfly(self):
        k = int(log(self.n, 2))
        num_switches_per_layer = 2 ** (k - 1)
        start_layers = self.n
        end_layers = start_layers + k * num_switches_per_layer
        # Change number of nodes to that with internal switches
        self.num_nodes = (self.n//2)*(k + 4)

        # Add the remaining internl nodes, or rather switches.
        for i in range(start_layers,end_layers,1):
            index = i - start_layers
            temp = node(name = self.name + "_switch_" + str(index//num_switches_per_layer) + "_" + format(index%num_switches_per_layer, "0" + str(k-1) + "b"), ID = self.node_start_id + i, tile_ID = i)
            self.nodes.insert(i, temp)

        # Make the connections between the switches.
        # We organize the switches into layers and iterate over them.
        # The layer we are currently working with decides which bit to consider for making the connection to the next layer.
        for layer in range(k-1):
            for switch in range(self.n//2):
                bitstring = format(switch, "0" + str(k - 1) + "b")
                # Find out which is the node to connect to, other than the one of the same number(bit representation), in the next layer.
                other_switch = int(bitstring[0:layer] + format(not eval(bitstring[layer]), "01b") + bitstring[layer+1:], 2)
                # There are totally 4 links for each switch in the network
                self.nodes[start_layers + layer * num_switches_per_layer + switch].head.links.append(self.nodes[start_layers + (layer + 1) * num_switches_per_layer + switch].head)
                self.nodes[start_layers + (layer + 1) * num_switches_per_layer + switch].head.links.append(self.nodes[start_layers + layer * num_switches_per_layer + switch].head)
                self.nodes[start_layers + layer * num_switches_per_layer + switch].head.links.append(self.nodes[start_layers + (layer + 1) * num_switches_per_layer + other_switch].head)
                self.nodes[start_layers + (layer + 1) * num_switches_per_layer + other_switch].head.links.append(self.nodes[start_layers + layer * num_switches_per_layer + switch].head)

        # Add links for the input and output nodes.
        for i in range(self.n):
            switch = int(i/2)
            self.nodes[i].head.links.append(self.nodes[start_layers + switch].head)
            self.nodes[start_layers + switch].head.links.append(self.nodes[i].head)
            self.nodes[end_layers + i].head.links.append(self.nodes[end_layers - num_switches_per_layer + switch].head)
            self.nodes[end_layers - num_switches_per_layer + switch].head.links.append(self.nodes[end_layers + i].head)

        # Choose the head as the middle node in the output layer(as discussed in class)
        self.head = self.nodes[end_layers + self.n//2].head

def check_input(line):
    if len(line) != 3:
        print("Incorrect format! Must include: X,n,m !")
        exit(1)

    X = line[0]
    if X not in ["C","R","M","F","H","B"]:
        print("Incorrect option for topology type!")
        exit(2)

    n, m = line[1:]
    try:
        n = int(n)
        m = int(m)
    except:
        print("Dimensions are not of integer type!")
        exit(3)

    if X == "C":
        if m != 1:
            print("Dimension m for chain must be 1! Ignoring and continuing!")

    elif X == "R":
        if m != 1:
            print("Dimension m for chain must be 1! Ignoring and continuing!")

    elif X == "H":
        if n != m or n != 3:
            print("Dimensions for hypercube must be 3 and equal! Ignoring and continuing!")

    elif X == "B":
        b = format(n, "0b")
        if n != m or b.count("1") != 1:
            print("Dimensions for butterfly must be equal and power of 2! Exiting!")
            exit(4)

def write_node_to_outfile(out_file = None, tile_node = None):
    if out_file == None:
        print("Please provide output file!")
        return
    if type(tile_node) != node:
        print("Please give node of type node to write!")
        return

    out_file.write("\n*****")
    out_file.write("\nNode Name: " + tile_node.name)
    out_file.write("\nNode ID: " + str(tile_node.id))
    out_file.write("\nTile ID: " + str(tile_node.tile_ID))
    out_file.write("\nLinks: " + str(len(tile_node.links)))
    for link_number, links in enumerate(tile_node.links):
        out_file.write("\n\tL(" + str(link_number + 1) + "):" + str(links.id))
    out_file.write("\n*****")


def route_within_tileL1(start, end, tile):
    route = []
    if tile.net_type == 'C':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(tile.nodes[a].head.id)
        while a != b:
          a, vc = route_Chain(a, b)
          print(tile.nodes[a].head.id, vc)
          route.append((tile.nodes[a].head.id, vc))
    elif tile.net_type == 'R':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(tile.nodes[a].head.id)
        vc = 0
        while a != b:
          a, vc = route_Ring(a, b, vc, tile.n)
          print(tile.nodes[a].head.id, vc)
          route.append((tile.nodes[a].head.id, vc))

    elif tile.net_type == 'M':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(tile.nodes[a].head.id)
        while a != b:
          a, vc = route_Mesh(a, b, tile.n)
          print(tile.nodes[a].head.id, vc)
          route.append((tile.nodes[a].head.id, vc))
    elif tile.net_type == 'F':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(tile.nodes[a].head.id)
        vc = 0
        while a != b:
          a, vc = route_FT(a, b, tile.n, tile.m, vc)
          print(tile.nodes[a].head.id, vc)
          route.append((tile.nodes[a].head.id, vc))
    elif tile.net_type == 'H':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(tile.nodes[a].head.id)
        while a != b:
          a, vc = route_Hypercube(a, b)
          print(tile.nodes[a].head.id, vc)
          route.append((tile.nodes[a].head.id, vc))
    elif tile.net_type == 'B':
        a = start
        b = end

        tile_max = tile.num_nodes-1
        #a = start-tile.node_start_id
        #b = end  -tile.node_start_id
        #print(tile.nodes[a].head.id)
        #while a != b:
        #  a, vc = route_Hypercube(a, b)
        #  print(tile.nodes[a].head.id, vc)
        #print('a,b ',a,b)

        c, vc = route_Butterfly(a, b, tile.n)
        c2 = None
        #print(a)
        #print('hi')
        #for i in range(len(tile.nodes)):
        #  print(tile.nodes[i].head.id, len(tile.nodes))
        #print('hi')
        if a <tile.n and b < tile.n:
          c, vc = route_Butterfly(a,tile_max, tile.n)
          c2, vc = route_Butterfly(tile_max, b, tile.n)
        if a >=tile_max+1-tile.n and b >= tile_max-tile.n+1:
          c, vc = route_Butterfly(a,0, tile.n)
          c2, vc = route_Butterfly(0, b, tile.n)



        for i in (c):
        #for i in range(len(a)):
          print(tile.nodes[i].head.id, 0)
          route.append((tile.nodes[i].head.id, 0))
        if c2:
            for i in ((c2)):
                #print(vc[i])
                print(tile.nodes[i].head.id, 0)
                #print(c2[i]+tile.node_start_id, vc[i])
                route.append((tile.nodes[i].head.id, 0))
                #route.append((c2[i]+tile.node_start_id, vc[i]))


    return route


def route_within_tile(start, end, tile):
    route = []
    if tile.net_type == 'C':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(a+tile.node_start_id)
        while a != b:
          a, vc = route_Chain(a, b)
          print(a+tile.node_start_id, vc)
          route.append((a+tile.node_start_id, vc))
    elif tile.net_type == 'R':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(a+tile.node_start_id)
        vc = 0
        while a != b:
          a, vc = route_Ring(a, b, vc, tile.n)
          print(a+tile.node_start_id, vc)
          route.append((a+tile.node_start_id, vc))
    elif tile.net_type == 'M':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(a+tile.node_start_id)
        while a != b:
          a, vc = route_Mesh(a, b, tile.n)
          print(a+tile.node_start_id, vc)
          route.append((a+tile.node_start_id, vc))
    elif tile.net_type == 'F':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(a+tile.node_start_id)
        vc = 0
        while a != b:
          a, vc = route_FT(a, b, tile.n, tile.m, vc)
          print(a+tile.node_start_id, vc)
          route.append((a+tile.node_start_id, vc))
    elif tile.net_type == 'H':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(a+tile.node_start_id)
        while a != b:
          a, vc = route_Hypercube(a, b)
          print(a+tile.node_start_id, vc)
          route.append((a+tile.node_start_id, vc))
    elif tile.net_type == 'B':
        a = start-tile.node_start_id
        b = end  -tile.node_start_id
        #print(a+tile.node_start_id)
        #while a != b:
        #  a, vc = route_Hypercube(a, b)
        #  print(a+tile.node_start_id, vc)
        tile_max = tile.num_nodes-1

        c, vc = route_Butterfly(a, b, tile.n)
        c2 = None
        #for i in a:
        #print(a, b, c, vc)
        if a <tile.n and b < tile.n:
          c, vc = route_Butterfly(a,tile_max, tile.n)
          c2, vc = route_Butterfly(tile_max, b, tile.n)
        if a >=tile_max+1-tile.n and b >= tile_max-tile.n+1:
          c, vc = route_Butterfly(a,0, tile.n)
          c2, vc = route_Butterfly(0, b, tile.n)

        for i in range(len(c)):
          #print(vc[i])
          print(c[i]+tile.node_start_id, vc[i])
          route.append((c[i]+tile.node_start_id, vc[i]))
        if c2:
            for i in range(len(c2)):
                #print(vc[i])
                print(c2[i]+tile.node_start_id, vc[i])
                route.append((c2[i]+tile.node_start_id, vc[i]))
    return route




if __name__ == "__main__":
    # Read from L1 file for first layer
    #l1_file_name = "L1Topology.txt"
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--L1', help='pass L1 topology file path')
    parser.add_argument('--L2', help='pass L2 topology file path')
    parser.add_argument('--O', help='pass output file path')
    #parser.add_argument('--Start', help='Starting node')
    #parser.add_argument('--End', help='Destination/End node')
    args = vars(parser.parse_args())
    l1_file_name = args["L1"]
    #l1_file_name = "L1Topology.txt"
    l1_file = open(l1_file_name, "r")
#    X, n, m = l1_file.readline().split(",")
    line = l1_file.readline().split(",")
    check_input(line)
    X = line[0]
    n = int(line[1])
    m = int(line[2])
    net = Net(net_type = X, n = n, m = m)
    l1_file.close()
    net.calc_num_nodes()

    # Read from L2 file for each tile
    l2_file_name = args['L2']
    #l2_file_name = "L2Topology.txt"
    #l2_file_name = "L2Topology.txt"
    l2_file = open(l2_file_name, "r")
    line = l2_file.readline().split(",")

    # Counter for tile id.
    tile_id = 0
    # Counter for number of tiles done
    num_tiles = 0
    # Counter for total number of nodes in the network.
    total_num_nodes = 0 

    # Create each tile.
    while(line[0] != ''):
        check_input(line)
        net_type, n_tile, m_tile = line
        n_tile = int(n_tile)
        m_tile = int(m_tile)

        if tile_id > net.num_nodes and net.net_type != "B":
            print("Too many tiles given as input for given L1topology!")
            exit(6)

        t = Net(ID = tile_id, node_start_id = total_num_nodes, net_type = net_type, n = n_tile, m = m_tile)
        # Add nodes to the tile.
        t.calc_num_nodes()
        t.add_nodes_to_tile()
        # Create the links to the tile.
        t.create_net()
        # Add the tile to the higher order network
        net.add_tile_to_net(t)
        # Keep track of total number of nodes made, to assign correct node_start_id
        total_num_nodes += t.num_nodes
        line = l2_file.readline().split(",")
        # Change tile ID to correspond to format used in butterfly
        if net.net_type == "B" and tile_id == n - 1:
            k = int(log(n, 2))
            tile_id += int((n/2) * k)
        # Keep track of tiles made
        tile_id += 1
        num_tiles += 1

    l2_file.close()

    if num_tiles != net.num_nodes:
        print("Not enough tiles to add to network! Exiting!")

    # Create links in the network, i.e. between tiles.
    else:
        # To number the switches different from the existing nodes
        if net.net_type == "B":
            net.node_start_id = total_num_nodes
        net.create_net()

        #startNodeID = args['Start']
        #endNodeID = args['End']
        startNodeID =100
        endNodeID = 7
        tilestart = None
        tileend = None

        for tile in net.nodes:
            #if 'switch' not in tile.name:
                #print(tile.head.tile_ID)
                if 'switch' not in tile.name:
                    print("Tile ID: ", tile.id, '   Tile Head ID:', tile.head.id, "   Tile Type: ", tile.net_type)
                    #print("Tile ID:", tile.id, tile.node_start_id, tile.head.id, tile.net_type)
                    nums = [i.id for i in (tile.nodes)]
                    print("Nodes in L2 layer: ", nums)
                    if startNodeID in nums:
                      tilestart = tile
                    if endNodeID in nums:
                      tileend = tile
                else:
                    print("Tile ID: ", tile.head.tile_ID, "  Switch Node ID",tile.id)

        #print(tilestart.id, tileend.id)
        #print(tilestart.node_start_id, tileend.node_start_id)
        #print(tilestart.head.id, tileend.head.id)

        #print('hi')
        r1 = []
        r3 = []
        print('')
        print('START')
        print('')
        print('Starting Node: ', startNodeID)
        if tilestart.id == tileend.id:
            route_within_tile(startNodeID, endNodeID, tilestart)
        else:
            print('Routing within starting tile')
            if startNodeID == tilestart.head.id:
              pass
            else:
              r1 = route_within_tile(startNodeID, tilestart.head.id, tilestart)
            #L1nums = [tile.id for i in net.nodes]
            print('Routing between tiles')
            r2 = route_within_tileL1(tilestart.head.tile_ID, tileend.head.tile_ID, net)
            #route_within_tileL1(tilestart.id, tileend.id, net)

            print('Routing within destination tile')
            if tileend.head.id == endNodeID:
                print('End Node: ', r2[-1])
            else:
                r3 = route_within_tile(tileend.head.id, endNodeID, tileend)
                print('End Node: ', r3[-1])

            #for node in tile.nodes:
            #  print(node.id)

        ## Write to file.
        ##out_file_name = "Network.txt"
        out_file_name = args['O']
        out_file = open(out_file_name, "w")

        for tile in net.nodes:
            if type(tile) == Net:
                for tile_node in tile.nodes:
                    write_node_to_outfile(out_file = out_file, tile_node = tile_node)
            else:
                write_node_to_outfile(out_file = out_file, tile_node = tile)

        out_file.close()

    # Debugging purposes
#    for i in net.nodes:
#        print(i.name+":Head = "+i.head.name+":")
#        for j in i.nodes:
#            print(j.name+":")
#            for k in j.links:
#                print(k.name, k.id)
