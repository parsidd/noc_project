# NOC_project

Repository containing files for NOC project of CS6230 July 2021 for team **Flip-flops**.

### Each project folder will have its own README as well, specific to that project. Please refer to them for more details about each project.

Members:
| Name | Roll number|
| ------ | ------ |
| Mansi Choudhary | EE17B053 |
| Vighnesh Natarajan | EE17B119 |
| Parth S Shah | EE17B059 |
