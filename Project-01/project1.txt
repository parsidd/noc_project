Deadline - Sept 18 2021

Complete details explained in the Lecture 18 conducted on Sept 03, 2021

You can look at 18LecSep03_P4.mp4

The project is to create a two-level hierarchical Network on Chip using any programming language of your choice.

------------------------------------INPUT----------------------------

The two input files:

 L1Topology.txt will have single line with this data

X,n,m where X = C, R, M, F, H, B

C: Chain, R: Ring, M: Mesh, F: Folded Torus, H: Hypercube of dimension 3 (8 nodes), B: Butterfly network

n: Number of nodes in first dimension

m: Number of nodes in second dimension - it is 1 for C and R.

n = m = 3 for H and 

n = m = 2^k  (Power of 2) for B for some k > 1

L2Topology.txt will have n X m lines with each line as the same syntax as above.

------------------------------------------------------

-------------OUTPUT---------------------------------------

All nodes in a single file. Each node described in the form

******

NodeID: 

Links: <Number of links> (say P of them)

P lines of this format <L(i): Destination Node> 1<= i <= P

**********

------------------------------------------------