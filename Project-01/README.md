# PROJECT 01

This folder contains the files for project01.

1. [project1.txt](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project1.txt)                     : Contains the problem statement as given to us.
2. [project01.py](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project01.py)                     : Python code, which is our solution.
3. [Testcases](https://gitlab.com/parsidd/noc_project/-/tree/main/Project-01/TestCases)  : Folder with testcases to the code which specify the topologies present in the network.
4. [Output](https://gitlab.com/parsidd/noc_project/-/tree/main/Project-01/Output)                      : Folder with outputs from the python code which lists each node and their links.
<!-- 5. [test.py](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/test.py) : File that is used as for testing correctness of output. -->

## Usage Syntax
python project01.py --L1 TestCases/Test1_L1.txt --L2 TestCases/Test1_L2.txt --O Output/Network.txt

Any version of python >=3.0 should work.

## Useage
python3.9 project01.py --L1 L1Topology.txt --L2 L2Topology.txt

## Input format:
The L1 file will contain only 1 line, which specifies the higher order topology of the network. This line will be the form:
`X, n, m`, where the notation is the same as given in the [problem statement](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project1.txt). The L2 file describes each "tile"  of the network as we call it, which itself is a network of nodes. Each line in the L2 file will be of the same format as that of the single line in the L1 file. **Note** that we require each line to follow the correct format and must have all three parameters; in case of a malformatted file, the code will send a message on the standard output terminal, indicating the error faced or assumption made.

## Output format:
The output file describes the graph as required by the problem statement. The format followed for each node is described below:
```
*****
Node Name: <Node Name>
Node ID: <Node ID>
Links: <Number of Links>
	L(1):<Node connected through first link>
    L(2):<Node connected through second link>
    ...
    ...
    L(i):<Node connected through i'th link>
    ...
*****
```
`Node name` follows the following format: the first letter specifies the type of tile topology the node belongs to. The next number specifies the ID number of the tile in the larger network. Note that this `tile ID` is assigned based on the order in the L2 file. 
For Chain, Ring, and Hypercube, tile topologies, the next number in the `node name` indicates the node number in the tile itself. For Mesh and Folded Torus, the next two numbers are indices in a 2D plane. For the Butterfly tiles, there is a substring that follows which specifies whether the node is an input to the Butterfly tile, a switch, or an output node to the Butterfly tile. 

The `node ID` is a global ID assigned to each node, in order of creation. The links use this node ID to indicate the connections. 

## Proposed testing strategy
Since the output format was not exactly clear (eg. Node ID/notation etc), we can't have a uniform testing strategy for all submissions. This strategy will work for our strategy as we can take advantage of the 'Node name' field, to save the data cleverly.
- We can extract all the different level 2 graphs from the output file based on the name - they are stored contiguously with the first character representing the net type and the second character the level 2 net number. 
- Find the node from the list of links that has only a single link and is at the expected L1 connection position, that is the node that connects this graph to an outer level graph. 
- Now that we know which are the level 1 links, we can probe the level 2 nets. 
- This requires a different strategy for the different topologies.
- From our naming convention, we can extract the dimensions of the sub net.
- For chain and ring, start two pointers on the links and have them move in opposite directions (can be judged based on node ID). If they do not meet, then it is a chain, else its a ring.
- For mesh and folded torus, repeat the same as above but in 2 dimensions. Additionally can verify the number of links for the different nodes. For mesh that will lie between 2,3,4 (once we remove the link corresponding to the L1 graph). For folded torus they will all be 4.
- For hypercube we can go through the nodes in order of their node ID, and since we know that each node must be connected to every other node whose node ID is away by only a Hamming distance of 1 (by comparing the binary representations of the node IDs), it is easy enough to confirm this.
- For a butterfly network, we must confirm first whether the input and output nodes are connected to the correct switches, which is again done based on their node ID. The lowest 2 node ID on the input side connect to the first switch in the first layer, the next 2 to the the next switch of the first layer, and so on. Similarly for the output side. We can easily check which nodes are input, output or switches based on the name we've given in our code, which clearly mentions the type of node (inp, out, or switch). For the switches, the name also includes which layer/stage it belongs to in the butterfly network, and using this we can figure out which bit in the binary representation of node number(part of the name) to consider while checking the connections. 

## Authors:
**Team Flip-flops:**
| Name | Roll number|
| ------ | ------ |
| Mansi Choudhary | EE17B053 |
| Vighnesh Natarajan | EE17B119 |
| Parth S Shah | EE17B059 |

