`include "parameter.bsv"
`include "network.bsv"
//`include "node.bsv"

module mkTB_NoC();
    Reg#(Bit#(10)) clock_val <- mkReg(0);
    let mynetwork <- mkNetwork;
    //let mynode <- mk_node(3'b0, 1, 4, 2, 8'b00000000, 2, 0, False, 1, -1, -1, -1, -1, -1, -1, -1);

    rule rl_clock(clock_val < 1000);
      clock_val <= clock_val + 1;
    endrule

    // rule rl_clk(clock_val%4 == 0);
    //   $display(clock_val);
    // endrule
  
    rule rl_finish(clock_val == 1000);
      $finish;
    endrule

    rule rl_put_flit(clock_val==4);
      //mynode.gen_flit(3,8'b0,5);
      int startAddr = 0;
      int destAddr = 15;
      Bit#(8) destTaddr = 0;
      int data = 5;
      mynetwork.top_gen_flit(startAddr,destAddr,destTaddr,data);
    endrule

  endmodule
