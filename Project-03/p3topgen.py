# NOC project part 1

# Import log for butterfly network
from math import log


# Class to define a single node in the NOC.
# Attributes include:
# 1. head : the head of the node, which is itself
# 2. name : the name of the node
# 3. id : global id number of the node
# 4. links : the links of the node as a list of nodes it connects to
class node:
    def __init__(self, ID = 0, name ="", tile_ID = -1, tile_head = -1):
        self.head = self
        self.id = ID
        self.tile_ID = tile_ID
        self.tile_head = tile_head
        self.name = name
        self.links = []

# Class to define a network. This may be used as a tile in the complete network
# or even as the entire network. Extendable to multiple layered networks.
# Attributes include:
# 1. n : size of1st dimension of the network
# 2. m : size of 2nd dimension of the network
# 3. node_start_id : the id of (lexicographically) first node in the network
# 4. name : name of the network
# 5. net_type : type of network:Eg. ring
# 6. nodes : list of nodes in the network
# 7. num_nodes : total number of nodes in the network
# 8. head : defines the head of the network, which will connect to other networks when used as a tile in a higher order network.

class Net:
    def __init__(self, ID = 0, node_start_id = 0, net_type = "C", n = 1, m = 1, tile_start_ID = 0):
        self.n = n
        self.m = m
        self.id = ID
        self.node_start_id = node_start_id
        self.tile_start_ID = tile_start_ID
        self.tile_head = 0
        self.name = net_type + str(ID)
        self.net_type = net_type
        self.nodes = []
        self.num_nodes = 0
        # Do not assign head yet. Only once we create the links.
        self.head = None

    # Function to add a tile(of class Net) to the higher order network(of class Net)
    def add_tile_to_net(self, tile):
        self.nodes.append(tile)

    # Calculate number of nodes for each net.
    def calc_num_nodes(self):
        if self.net_type == "C":
            self.num_nodes  = self.n

        elif self.net_type == "R":
            self.num_nodes  = self.n

        elif self.net_type == "M":
            self.num_nodes = self.n * self.m;

        elif self.net_type == "F":
            self.num_nodes = self.n * self.m;

        elif self.net_type == "H":
            self.num_nodes = 2**3

        elif self.net_type == "B":
            # For the butterfly network we add only the input output nodes for now.
            # The internal nodes, or switches, will be added while creating the network.
            self.num_nodes = 2*self.n

    # Function to add nodes to a tile in a network based on the type of network.
    # To be used explicitly for a tile, which is a network of 2nd order
    def add_nodes_to_tile(self):
        if self.net_type == "C":
#            self.num_nodes  = self.n
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "R":
#            self.num_nodes  = self.n
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "M":
#            self.num_nodes = self.n * self.m;
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i%self.n) + str(i//self.n), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "F":
#            self.num_nodes = self.n * self.m;
            for i in range(self.num_nodes):
                temp = node(name = self.name + "_" + str(i%self.n) + str(i//self.n), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "H":
#            self.num_nodes = 2**3
            for i in range(2**3):
                temp = node(name = self.name + "_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                self.nodes.append(temp.head)

        elif self.net_type == "B":
            # For the butterfly network we add only the input output nodes for now.
            # The internal nodes, or switches, will be added while creating the network.
            k = int(log(self.n, 2))
            num_switches_per_layer = 2 ** (k - 1)
#            self.num_nodes = (self.n//2)*(k + 4)
            start_layers = self.n
            end_layers = start_layers + k * num_switches_per_layer

            for i in range(2*self.n):
                if i < start_layers:
                    temp = node(name = self.name + "_inp_" + str(i), ID = self.node_start_id + i, tile_ID = tile_id)
                    self.nodes.append(temp.head)
#                elif i < end_layers:
#                    index = i - start_layers
#                    temp = node(name = self.name + "_switch" + str(index//num_switches_per_layer) + str(index%num_switches_per_layer), ID = self.node_start_id + i)
#                    self.nodes.append(temp.head)
                else:
                    temp = node(name = self.name + "_out_" + str(i - start_layers), ID = self.node_start_id + end_layers - self.n +i, tile_ID = tile_id)
                    #
                    self.nodes.append(temp.head)

    # Wrapper function to call the appropriate function to create the links between the nodes in the network
    def create_net(self):
        if self.net_type == "C":
            self.create_chain()

        elif self.net_type == "R":
            self.create_ring()

        elif self.net_type == "M":
            self.create_mesh()

        elif self.net_type == "F":
            self.create_foldedTorus()

        elif self.net_type == "H":
            self.create_hypercube()

        elif self.net_type == "B":
            self.create_butterfly()

    # Function to create links between nodes in a chain.
    def create_chain(self):
        for i in range(self.n - 1):
            # We only need to create a link between adjacent nodes, in only one dimension.
            self.nodes[i].head.links.append(self.nodes[i+1].head)
            self.nodes[i+1].head.links.append(self.nodes[i].head)

        # Assign the middle node as the head of the network.
        self.head = self.nodes[self.n//2].head
        for i in self.nodes:
            i.tile_head = self.head

    # Function to create links between nodes in a ring
    def create_ring(self):
        for i in range(self.n):
            # We need to make all the links as a chain, and additionally the link between the last first node. Taking mod makes this easier to do.
            self.nodes[i].head.links.append(self.nodes[(i+1)%(self.n)].head)
            self.nodes[(i+1)%self.n].head.links.append(self.nodes[i].head)

        # The ring is symmetric, hence it doesn't matter which node we choose as head
        self.head = self.nodes[0].head
        for i in self.nodes:
            i.tile_head = self.head

    # Function to create links between nodes in a mesh
    def create_mesh(self):
        for i in range(self.n):
            for j in range(self.m):
                # Create links in the first dimension. Is the same as a chain.
                if i < self.n - 1:
                    self.nodes[i + j * self.n].head.links.append(self.nodes[i + j * self.n + 1].head)
                    self.nodes[i + j * self.n + 1].head.links.append(self.nodes[i + j * self.n].head)
                # Create links in second dimension. It's like a chain in the other dimension also.
                if j < self.m - 1:
                    self.nodes[i + j * self.n].head.links.append(self.nodes[i + (j + 1) * self.n].head)
                    self.nodes[i + (j + 1) * self.n].head.links.append(self.nodes[i + j * self.n].head)

        # Choose the middle node of the mesh as the head.
        self.head = self.nodes[self.n//2 + (self.m//2) * self.n].head
        for i in self.nodes:
            i.tile_head = self.head

    # Function to create links between nodes in a folded torus.
    def create_foldedTorus(self):
        for i in range(self.n):
            for j in range(self.m):
                # Create links in the first dimension. Is the same as a chain.
                if i < self.n - 1:
                    self.nodes[i + j * self.n].head.links.append(self.nodes[i + j * self.n + 1].head)
                    self.nodes[i + j * self.n + 1].head.links.append(self.nodes[i + j * self.n].head)
                    # print("i Linking between ", self.nodes[i + j * self.n].id,  self.nodes[i + j * self.n + 1].id)
                # Create links in second dimension. It's like a chain in the other dimension also.
                if j < self.m - 1:
                    self.nodes[i + j * self.n].head.links.append(self.nodes[i + (j + 1) * self.n].head)
                    self.nodes[i + (j + 1) * self.n].head.links.append(self.nodes[i + j * self.n].head)
                    # print("j Linking between ", self.nodes[i + (j + 1) * self.n].id,  self.nodes[i + j * self.n].id)

        if self.m > 2:
            for i in range(self.n):
                self.nodes[i + (self.m - 1) * self.n].head.links.append(self.nodes[i].head)
                self.nodes[i].head.links.append(self.nodes[i  + (self.m - 1) * self.n].head)
        
        if self.n > 2:
            for j in range(self.m):
                self.nodes[(self.n - 1) + j * (self.n)].head.links.append(self.nodes[j * self.n].head)
                self.nodes[j * (self.n)].head.links.append(self.nodes[(self.n - 1) + j * self.n].head)
    
        # Again choose the middle node of the network as the head
        self.head = self.nodes[self.n//2 + (self.m//2) * self.n].head
        # 
        for i in self.nodes:
            i.tile_head = self.head

    # Function to create links between nodes in a hypercube(of dimension 3)
    def create_hypercube(self):
        # Need to create a simple cube connection. Adjacent nodes are of one Hamming distance away.
        for i in range(2**3):
            bitstring = format(i,"03b")
            n1 = format(not eval(bitstring[0]), "01b") + bitstring[1:]
            n2 = bitstring[0] + format(not eval(bitstring[1]), "01b") + bitstring[2]
            n3 = bitstring[0:2] + format(not eval(bitstring[2]), "01b")
            self.nodes[i].head.links.append(self.nodes[int(n1, 2)].head)
            self.nodes[i].head.links.append(self.nodes[int(n2, 2)].head)
            self.nodes[i].head.links.append(self.nodes[int(n3, 2)].head)

        # The hypercube is also a symmetric structure, so we can choose any node as the head.
        self.head = self.nodes[0].head
        for i in self.nodes:
            i.tile_head = self.head

    # Function to create links between nodes in a butterfly network
    def create_butterfly(self):
        k = int(log(self.n, 2))
        num_switches_per_layer = 2 ** (k - 1)
        start_layers = self.n
        end_layers = start_layers + k * num_switches_per_layer
        # Change number of nodes to that with internal switches
        self.num_nodes = (self.n//2)*(k + 4)

        # Add the remaining internl nodes, or rather switches.
        for i in range(start_layers,end_layers,1):
            index = i - start_layers
            temp = node(name = self.name + "_switch_" + str(index//num_switches_per_layer) + "_" + format(index%num_switches_per_layer, "0" + str(k-1) + "b"), ID = self.node_start_id + i, tile_ID = -1)
            self.nodes.insert(i, temp)

        # Make the connections between the switches.
        # We organize the switches into layers and iterate over them.
        # The layer we are currently working with decides which bit to consider for making the connection to the next layer.
        for layer in range(k-1):
            for switch in range(self.n//2):
                bitstring = format(switch, "0" + str(k - 1) + "b")
                # Find out which is the node to connect to, other than the one of the same number(bit representation), in the next layer.
                other_switch = int(bitstring[0:layer] + format(not eval(bitstring[layer]), "01b") + bitstring[layer+1:], 2)
                # There are totally 4 links for each switch in the network
                self.nodes[start_layers + layer * num_switches_per_layer + switch].head.links.append(self.nodes[start_layers + (layer + 1) * num_switches_per_layer + switch].head)
                self.nodes[start_layers + (layer + 1) * num_switches_per_layer + switch].head.links.append(self.nodes[start_layers + layer * num_switches_per_layer + switch].head)
                self.nodes[start_layers + layer * num_switches_per_layer + switch].head.links.append(self.nodes[start_layers + (layer + 1) * num_switches_per_layer + other_switch].head)
                self.nodes[start_layers + (layer + 1) * num_switches_per_layer + other_switch].head.links.append(self.nodes[start_layers + layer * num_switches_per_layer + switch].head)

        # Add links for the input and output nodes.
        for i in range(self.n):
            switch = int(i/2)
            self.nodes[i].head.links.append(self.nodes[start_layers + switch].head)
            self.nodes[start_layers + switch].head.links.append(self.nodes[i].head)
            self.nodes[end_layers + i].head.links.append(self.nodes[end_layers - num_switches_per_layer + switch].head)
            self.nodes[end_layers - num_switches_per_layer + switch].head.links.append(self.nodes[end_layers + i].head)

        # Choose the head as the middle node in the output layer(as discussed in class)
        self.head = self.nodes[end_layers + self.n//2].head
        for i in self.nodes:
            i.tile_ID = self.head.tile_ID
            i.tile_head = self.head

def check_input(line):
    if len(line) != 3:
        print("Incorrect format! Must include: X,n,m !")
        exit(1)

    X = line[0]
    if X not in ["C","R","M","F","H","B"]:
        print("Incorrect option for topology type!")
        exit(2)

    n, m = line[1:]
    try:
        n = int(n)
        m = int(m)
    except:
        print("Dimensions are not of integer type!")
        exit(3)

    if X == "C":
        if m != 1:
            print("Dimension m for chain must be 1! Ignoring and continuing!")

    elif X == "R":
        if m != 1:
            print("Dimension m for chain must be 1! Ignoring and continuing!")

    elif X == "H":
        if n != m or n != 3:
            print("Dimensions for hypercube must be 3 and equal! Ignoring and continuing!")

    elif X == "B":
        b = format(n, "0b")
        if n != m or b.count("1") != 1:
            print("Dimensions for butterfly must be equal and power of 2! Exiting!")
            exit(4)

def add_node_to_bsv(net, tile, tile_node = None):
    head = False
    topology = 0
    l1topology = 0

    if(net.net_type=="C"):
        l1topology = bin(0)[2:]
    elif(net.net_type=="R"):
        l1topology = bin(1)[2:]
    elif(net.net_type=="M"):
        l1topology = bin(2)[2:]
    elif(net.net_type=="F"):
        l1topology = bin(3)[2:]
    elif(net.net_type=="H"):
        l1topology = bin(4)[2:]
    elif(net.net_type=="B"):
        l1topology = bin(5)[2:]


    if(tile.head.id==tile_node.id):
        head = True

    if(tile.net_type=="C"):
        topology = bin(0)[2:]
    elif(tile.net_type=="R"):
        topology = bin(1)[2:]
    elif(tile.net_type=="M"):
        topology = bin(2)[2:]
    elif(tile.net_type=="F"):
        topology = bin(3)[2:]
    elif(tile.net_type=="H"):
        topology = bin(4)[2:]
    elif(tile.net_type=="B"):
        topology = bin(5)[2:]

    links_str = ""
    heads_str = ""
    tiles_str  = ""

    total_num_links = 8

    for i in range(total_num_links):
        if i < len(tile_node.links):
            links_str = links_str + str(tile_node.links[i].id)
            heads_str = heads_str + str(tile_node.links[i].tile_head.id)
            tiles_str  = tiles_str + str(tile_node.links[i].tile_ID)
        else:
            links_str = links_str + "-1"
            heads_str = heads_str + "-1"
            tiles_str = tiles_str + "-1"

        if i < total_num_links - 1:
            links_str = links_str + ", "
            heads_str = heads_str + ", "            
            tiles_str = tiles_str + ", "            


    fo.write("\n    node["+str(tile_node.id)+"] <- mk_node(3\'b"+l1topology+", " + str(net.m) + ", " + str(net.n) + ", " + "3\'b" + topology + ", " + str(tile.m) + ", " + str(tile.n) + ", " + str(tile.head.id)+", 8\'b" + format(tile.id, "08b") + ", " +str(tile_node.id)+", "+ str(tile.node_start_id)+", "+str(head)+", " + links_str + ", " + heads_str + ", " + tiles_str + ");\n")

def add_link_to_bsv(tile_node = None):
    for link_number, links in enumerate(tile_node.links):
        link_number_back = 0
        for linkNumBack, l in enumerate(links.links):
            if l.id == tile_node.id:
                link_number_back = linkNumBack
        fo.write("\n    rule link"+str(tile_node.id)+str(links.id)+" ;")
        fo.write("\n        Bit#(`Flit_width) myflit <- node["+str(links.id)+"].ma_get_flit"+str(link_number_back)+"();")
        fo.write("\n        node["+str(tile_node.id)+"].ma_put_flit"+str(link_number)+"(myflit);")
        fo.write("\n    endrule\n")
        #print(link_number, links.id)


def write_output(out_file = None, tile_node = None):
    if out_file == None:
        print("Please provide output file!")
        return
    if type(tile_node) != node:
        print("Please give node of type node to write!")
        return

    out_file.write("\n*****")
    out_file.write("\nNode Name: " + tile_node.name)
    out_file.write("\nNode ID: " + str(tile_node.id))
    out_file.write("\nLinks: " + str(len(tile_node.links)))

    for link_number, links in enumerate(tile_node.links):
        out_file.write("\n\tL(" + str(link_number + 1) + "):" + str(links.id))
    out_file.write("\n*****")




if __name__ == "__main__":
    # Read from L1 file for first layer
    #l1_file_name = "L1Topology.txt"
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--L1', help='pass L1 topology file path')
    parser.add_argument('--L2', help='pass L2 topology file path')
    parser.add_argument('--O', help='pass output file path')
    args = vars(parser.parse_args())

    #File pointer to output file
    fo    =   open("network.bsv","w")
    #write bsv code

    fo.write("`include \"parameter.bsv\" \n`include \"node.bsv\"\n")
    fo.write("\ninterface Ifc_Network; \n")
    fo.write("    method Action top_gen_flit(int src, int dest, Bit#(8) dest_tile, int data); \n")
    fo.write("endinterface \n")
    fo.write("\n(*synthesize*)\n")
    fo.write("\nmodule mkNetwork(Ifc_Network);\n")
    

    #l1_file_name = args["L1"]
    l1_file_name = "L1Topology.txt"
    l1_file = open(l1_file_name, "r")
#    X, n, m = l1_file.readline().split(",")
    line = l1_file.readline().split(",")
    check_input(line)
    X = line[0]
    n = int(line[1])
    m = int(line[2])
    net = Net(net_type = X, n = n, m = m)
    l1_file.close()
    net.calc_num_nodes()

    # Read from L2 file for each tile
    #l2_file_name = args['L2']
    l2_file_name = "L2Topology.txt"
    l2_file = open(l2_file_name, "r")
    line = l2_file.readline().split(",")

    # Counter for tile id.
    tile_id = 0
    # Counter for total number of nodes in the network.
    total_num_nodes = 0

    # Create each tile.
    while(line[0] != ''):
        check_input(line)
        net_type, n_tile, m_tile = line
        n_tile = int(n_tile)
        m_tile = int(m_tile)

        if tile_id > net.num_nodes:
            print("Too many tiles given as input for given L1topology!")
            exit(6)

        t = Net(ID = tile_id, node_start_id = total_num_nodes, net_type = net_type, n = n_tile, m = m_tile)
        # Add nodes to the tile.
        t.calc_num_nodes()
        t.add_nodes_to_tile()
        # Create the links to the tile.
        t.create_net()
        # Add the tile to the higher order network
        net.add_tile_to_net(t)
        # Keep track of total number of nodes made, to assign correct node_start_id
        total_num_nodes += t.num_nodes
        line = l2_file.readline().split(",")
        # Keep track of tiles made
        tile_id += 1

    l2_file.close()




    if tile_id != net.num_nodes:
        print("Not enough tiles to add to network! Exiting!")

    # Create links in the network, i.e. between tiles.
    else:
        net.create_net()

        # Write to file.
        out_file_name = "Network.txt"
        #out_file_name = args['O']
        out_file = open(out_file_name, "w")



        for tile in net.nodes:
            if type(tile) == Net:
                for tile_node in tile.nodes:
                    write_output(out_file = out_file, tile_node = tile_node)
            else:
                write_output(out_file = out_file, tile_node = tile)

        out_file.close()


    fo.write("\n    Vector#("+str(total_num_nodes)+", Ifc_Node) node;\n")
    
    for tile in net.nodes:
        if type(tile) == Net:
            for tile_node in tile.nodes:
                add_node_to_bsv(net, tile, tile_node = tile_node)
        else:
            add_node_to_bsv(net, tile, tile_node = tile)

    for tile in net.nodes:
        if type(tile) == Net:
            for tile_node in tile.nodes:
                add_link_to_bsv(tile_node = tile_node)
        else:
            add_link_to_bsv(tile_node = tile)

    fo.write("\n    method Action top_gen_flit(int src, int dest, Bit#(8) dest_tile, int data); \n")
    fo.write("        node[src].gen_flit(dest, dest_tile, data); \n")
    fo.write("    endmethod \n")

    fo.write("\nendmodule\n")
