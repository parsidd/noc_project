`include "parameter.bsv"
`include "router.bsv"

import FIFO::*;
import DReg::*;
import Vector::*;

typedef struct {
    Vector#(`VC_size, FIFO#(Bit#(`Flit_width))) router_fifo;
} Router_buffer;


interface Ifc_Node;
    method Action ma_put_flit0(Bit#(`Flit_width) flit);
    method Action ma_put_flit1(Bit#(`Flit_width) flit);
    method Action ma_put_flit2(Bit#(`Flit_width) flit);
    method Action ma_put_flit3(Bit#(`Flit_width) flit);
    method Action ma_put_flit4(Bit#(`Flit_width) flit);
    method Action ma_put_flit5(Bit#(`Flit_width) flit);
    method Action ma_put_flit6(Bit#(`Flit_width) flit);
    method Action ma_put_flit7(Bit#(`Flit_width) flit);
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit0;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit1;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit2;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit3;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit4;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit5;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit6;
    method ActionValue#(Bit#(`Flit_width)) ma_get_flit7;
    method Action gen_flit(int dest, Bit#(8) dest_tile, int data);
endinterface

(*synthesize*)
module mk_node#(Bit#(3) l1topology, int l1_m, int l1_n, Bit#(3) topology, int m, int n, int headid, Bit#(8) tileid, int nodeid, int startid, Bool head, int link0, int link1, int link2, int link3, int link4, int link5, int link6, int link7, int head0, int head1, int head2, int head3, int head4, int head5, int head6, int head7, int tile0, int tile1, int tile2, int tile3, int tile4, int tile5, int tile6, int tile7)(Ifc_Node);
    //store node information
    Bool headnode = head;
    Bit#(8) rg_tileid = tileid;
    int rg_nodeid = nodeid;
    int rg_startid = startid;
    int rg_headid = headid;
    //tile ids and head ids of links?

    Reg#(Bit#(3)) arb_cnt <- mkReg(0);

    // Reg#(Bool) rg_arbiter <- mkReg(False);

    Vector#(`IL_size, FIFO#(Bit#(`Flit_width))) in_fifo <- replicateM(mkFIFO);
    Vector#(`OL_size, FIFO#(Bit#(`Flit_width))) out_fifo <- replicateM(mkFIFO);

    Vector#(`IL_size, Ifc_Router) node_routers <- replicateM(mk_router);
    Vector#(`IL_size, Router_buffer) node_buffer; 
   
    for(Integer i=0; i<valueOf(`IL_size); i=i+1)
        node_buffer[i].router_fifo <- replicateM(mkFIFO);

    for(Integer i=0; i<valueOf(`IL_size); i=i+1) begin
        rule rl_router;
            let data = in_fifo[i].first;
            in_fifo[i].deq;
            //Format for data - 8'b prev vc, 8'b start tile id, 8'b prev tile id, 8'b dest tile id, 32'b start addr, 32'b prevAddr, 32'b currDest, 32'b finalDest, 32'b data (32'b == int?) `Flit_width = 192 bits
            let data32 = data[31:0];
            int finalDest = unpack(data[63:32]);
            int currDest = unpack(data[95:64]);
            int prevAddr = unpack(data[127:96]);
            int startAddr = unpack(data[159:128]);
            Bit#(8) destTile = data[167:160];
            Bit#(8) prevTile = data[175:168];
            Bit#(8) startTile = data[183:176];
            Bit#(8) prevVc = data[191:184];
            Out r_out;
            r_out.out_i=0;
            r_out.vc=prevVc;
            int g_loc = 0;

            $display("Current node id %h", rg_nodeid);
            if (rg_nodeid == finalDest) begin
                $display("Reached final destination node id %h", rg_nodeid);
                $display("Received flit %h", data32);
            end

            else begin
                if (prevAddr != currDest && rg_nodeid != currDest) begin
                    r_out <- node_routers[i].ma_route(topology, m, n, rg_nodeid - rg_startid, currDest - rg_startid, prevVc);
                    g_loc = unpack(r_out.out_i) + rg_startid;
                    $display("Initial tile route %h %h %h %h %h %h", r_out.out_i, g_loc, rg_nodeid, currDest, rg_startid, rg_headid);
                    //Initial tile route ffffffff 00000003 00000004 00000002 00000004

                end
                else if (rg_tileid != destTile) begin
                //else if (prevTile != destTile) begin
                    int nid = unpack(zeroExtend(rg_tileid));
                    int did = unpack(zeroExtend(destTile));
                    r_out <- node_routers[i].ma_route(l1topology, l1_m, l1_n, nid, did, prevVc);
                    g_loc = unpack(r_out.out_i);
                    if (g_loc == tile0) begin
                        g_loc = head0;
                    end
                    else if (g_loc == tile1) begin
                        g_loc = head1;
                    end
                    else if (g_loc == tile2) begin
                        g_loc = head2;
                    end
                    else if (g_loc == tile3) begin
                        g_loc = head3;
                    end
                    else if (g_loc == tile4) begin
                        g_loc = head4;
                    end
                    else if (g_loc == tile5) begin
                        g_loc = head5;
                    end
                    else if (g_loc == tile6) begin
                        g_loc = head6;
                    end
                    else if (g_loc == tile7) begin
                        g_loc = head7;
                    end
                    $display("Second route %h %h %h %h %h %h %h %h %h %h %h", rg_tileid, rg_startid, destTile, g_loc, r_out.out_i, nid, did, l1_m, l1_n, prevTile, l1topology);
                    //Second route 00 00000000 02 00000002 00000002 00000000 00000002 00000001 00000003 00 0
                    //Second route 00 00000000 02 00000002 00000002 00000000 00000002 00000003 00000003 00
                    //Second route 00 00000000 01 00000012 00000001 00000000 00000001 00000003 00000003 00
                    //Second route 00 00000000 01 00000005 00000001 00000000 00000001 00000001 00000004 00
                    //Second route 01 00000004 01 00000002 00000002 00000001 00000001 00000001 00000003 00

                    $display("%h, %h, %h, %h, %h, %h, %h, %h",tile0,tile1,tile2,tile3,tile4,tile5,tile6,tile7);
                end
                else begin
                    r_out <- node_routers[i].ma_route(topology, m, n, rg_nodeid - rg_startid, finalDest - rg_startid, prevVc);
                    g_loc = unpack(r_out.out_i) + rg_startid;
                    $display("Final route %h %h %h %h %h", rg_nodeid, rg_startid, finalDest, g_loc);
                    $display("%h, %h, %h, %h, %h, %h, %h, %h",link0,link1,link2,link3,link4,link5,link6,link7);
                    //Final route 00000012 00000008 0000000f 00000013 %h

                end

                if (prevAddr != currDest)
                  prevAddr = rg_nodeid;
                prevTile = rg_tileid;

                let out_flit = {prevVc, startTile, prevTile, destTile, pack(startAddr), pack(prevAddr), pack(currDest), pack(finalDest), data32};
                //let out_flit = 192'b0;
                int link = 7;

                if(g_loc!=-1 && g_loc==link0)
                    link = 0;
                
                else if(g_loc!=-1 && g_loc==link1)
                    link = 1;

                else if(g_loc!=-1 && g_loc==link2)
                    link = 2;

                else if(g_loc!=-1 && g_loc==link3)
                    link = 3;

                else if(g_loc!=-1 && g_loc==link4)
                    link = 4;

                else if(g_loc!=-1 && g_loc==link5)
                    link = 5;

                else if(g_loc!=-1 && g_loc==link6)
                    link = 6;

                else if(g_loc!=-1 && g_loc==link7)
                    link = 7;

                node_buffer[i].router_fifo[link].enq(out_flit);
                $display("buffer id %h ",i ,"loc val %h ", unpack(r_out.out_i), "g_loc val %h ", g_loc, "link val %h ", link);
                //$display("%h, %h, %h, %h, %h, %h, %h, %h",link0,link1,link2,link3,link4,link5,link6,link7);
            end

        endrule
    end

    for(Integer i=0; i<valueOf(`OL_size); i=i+1) begin
        rule rl_arbiter0(arb_cnt==0);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);    
            out_fifo[i].enq(node_buffer[0].router_fifo[i].first);
            node_buffer[0].router_fifo[i].deq;
        endrule
        rule rl_arbiter1(arb_cnt==1);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);    
            out_fifo[i].enq(node_buffer[1].router_fifo[i].first);
            node_buffer[1].router_fifo[i].deq;
        endrule
        rule rl_arbiter2(arb_cnt==2);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);    
            out_fifo[i].enq(node_buffer[2].router_fifo[i].first);
            node_buffer[2].router_fifo[i].deq;
        endrule
        rule rl_arbiter3(arb_cnt==3);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);      
            out_fifo[i].enq(node_buffer[3].router_fifo[i].first);
            node_buffer[3].router_fifo[i].deq;
        endrule
        rule rl_arbiter4(arb_cnt==4);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);    
            out_fifo[i].enq(node_buffer[4].router_fifo[i].first);
            node_buffer[4].router_fifo[i].deq;
        endrule
        rule rl_arbiter5(arb_cnt==5);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);   
            out_fifo[i].enq(node_buffer[5].router_fifo[i].first);
            node_buffer[5].router_fifo[i].deq;
        endrule
        rule rl_arbiter6(arb_cnt==6);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);   
            out_fifo[i].enq(node_buffer[6].router_fifo[i].first);
            node_buffer[6].router_fifo[i].deq;
        endrule
        rule rl_arbiter7(arb_cnt==7);
            $display("arbiter %h fired %h arbcnt", i, arb_cnt);    
            out_fifo[i].enq(node_buffer[7].router_fifo[i].first);
            node_buffer[7].router_fifo[i].deq;
        endrule
    end

    rule rl_arbiter_cnt;
        arb_cnt <= arb_cnt + 1;
    endrule

    rule display0;
        $display("in_fifo %h ",in_fifo[7].first);
    endrule

    // for(Integer i=0; i<valueOf(`OL_size); i=i+1) begin
    //     rule display1;
    //         $display("out_fifo %h %h ", i, out_fifo[i].first);
    //     endrule
    // end

    method Action ma_put_flit0(Bit#(`Flit_width) flit);
        in_fifo[0].enq(flit);
    endmethod

    method Action ma_put_flit1(Bit#(`Flit_width) flit);
        in_fifo[1].enq(flit);
    endmethod

    method Action ma_put_flit2(Bit#(`Flit_width) flit);
        in_fifo[2].enq(flit);
    endmethod

    method Action ma_put_flit3(Bit#(`Flit_width) flit);
        in_fifo[3].enq(flit);
    endmethod

    method Action ma_put_flit4(Bit#(`Flit_width) flit);
        in_fifo[4].enq(flit);
    endmethod

    method Action ma_put_flit5(Bit#(`Flit_width) flit);
        in_fifo[5].enq(flit);
    endmethod

    method Action ma_put_flit6(Bit#(`Flit_width) flit);
        in_fifo[6].enq(flit);
    endmethod

    method Action ma_put_flit7(Bit#(`Flit_width) flit);
        in_fifo[7].enq(flit);
    endmethod

    

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit0;
        $display("out_fifo %h", 0);
        Bit#(`Flit_width) outflit = out_fifo[0].first;
        out_fifo[0].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit1;
        $display("out_fifo %h", 1);
        Bit#(`Flit_width) outflit = out_fifo[1].first;
        out_fifo[1].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit2;
        $display("out_fifo %h", 2);
        Bit#(`Flit_width) outflit = out_fifo[2].first;
        out_fifo[2].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit3;
        $display("out_fifo %h", 3);
        Bit#(`Flit_width) outflit = out_fifo[3].first;
        out_fifo[3].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit4;
        $display("out_fifo %h", 4);
        Bit#(`Flit_width) outflit = out_fifo[4].first;
        out_fifo[4].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit5;
        $display("out_fifo %h", 5);
        Bit#(`Flit_width) outflit = out_fifo[5].first;
        out_fifo[5].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit6;
        $display("out_fifo %h", 6);
        Bit#(`Flit_width) outflit = out_fifo[6].first;
        out_fifo[6].deq;
        return outflit;
    endmethod

    method ActionValue#(Bit#(`Flit_width)) ma_get_flit7;
        $display("out_fifo %h", 7);
        Bit#(`Flit_width) outflit = out_fifo[7].first;
        out_fifo[7].deq;
        return outflit;
    endmethod

    method Action gen_flit(int dest, Bit#(8) dest_tile, int data);
        //Format for data - 8'b prev vc, 8'b start tile id, 8'b prev tile id, 8'b dest tile id, 32'b start addr, 32'b prevAddr, 32'b currDest, 32'b finalDest, 32'b data (32'b == int?) `Flit_width = 192 bits
        let tileStart = rg_tileid;
        let tilePrev = rg_tileid;
        let tileDest = dest_tile;
        let startAddr = pack(rg_nodeid);
        let prevAddr = pack(rg_nodeid);
        let currDest = pack(dest);
        if (dest_tile != rg_tileid) begin
            currDest = pack(rg_headid);
        end
        let finalDest = pack(dest);
        let flitdata = pack(data);
        let vc = 8'b0;

        let flit = {vc, tileStart, tilePrev, tileDest, startAddr, prevAddr, currDest, finalDest, flitdata};
        in_fifo[7].enq(flit);
        
    endmethod

endmodule
