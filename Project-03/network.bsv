`include "parameter.bsv" 
`include "node.bsv"

interface Ifc_Network; 
    method Action top_gen_flit(int src, int dest, Bit#(8) dest_tile, int data); 
endinterface 

(*synthesize*)

module mkNetwork(Ifc_Network);

    Vector#(28, Ifc_Node) node;

    node[0] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 0, 0, False, 1, 4, -1, -1, -1, -1, -1, -1, 10, 10, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1);

    node[1] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 1, 0, False, 0, 2, 5, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[2] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 2, 0, False, 1, 3, 6, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[3] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 3, 0, False, 2, 7, -1, -1, -1, -1, -1, -1, 10, 10, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1);

    node[4] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 4, 0, False, 0, 5, 8, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[5] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 5, 0, False, 4, 1, 6, 9, -1, -1, -1, -1, 10, 10, 10, 10, -1, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1);

    node[6] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 6, 0, False, 5, 2, 7, 10, -1, -1, -1, -1, 10, 10, 10, 10, -1, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1);

    node[7] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 7, 0, False, 6, 3, 11, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[8] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 8, 0, False, 4, 9, 12, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[9] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 9, 0, False, 8, 5, 10, 13, -1, -1, -1, -1, 10, 10, 10, 10, -1, -1, -1, -1, 0, 0, 0, 0, -1, -1, -1, -1);

    node[10] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 10, 0, True, 9, 6, 11, 14, 18, -1, -1, -1, 10, 10, 10, 10, 18, -1, -1, -1, 0, 0, 0, 0, 1, -1, -1, -1);

    node[11] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 11, 0, False, 10, 7, 15, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[12] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 12, 0, False, 8, 13, -1, -1, -1, -1, -1, -1, 10, 10, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1);

    node[13] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 13, 0, False, 12, 9, 14, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[14] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 14, 0, False, 13, 10, 15, -1, -1, -1, -1, -1, 10, 10, 10, -1, -1, -1, -1, -1, 0, 0, 0, -1, -1, -1, -1, -1);

    node[15] <- mk_node(3'b0, 1, 3, 3'b10, 4, 4, 10, 8'b00000000, 15, 0, False, 14, 11, -1, -1, -1, -1, -1, -1, 10, 10, -1, -1, -1, -1, -1, -1, 0, 0, -1, -1, -1, -1, -1, -1);

    node[16] <- mk_node(3'b0, 1, 3, 3'b0, 1, 4, 18, 8'b00000001, 16, 16, False, 17, -1, -1, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1);

    node[17] <- mk_node(3'b0, 1, 3, 3'b0, 1, 4, 18, 8'b00000001, 17, 16, False, 16, 18, -1, -1, -1, -1, -1, -1, 18, 18, -1, -1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, -1, -1);

    node[18] <- mk_node(3'b0, 1, 3, 3'b0, 1, 4, 18, 8'b00000001, 18, 16, True, 17, 19, 10, 26, -1, -1, -1, -1, 18, 18, 10, 26, -1, -1, -1, -1, 1, 1, 0, 2, -1, -1, -1, -1);

    node[19] <- mk_node(3'b0, 1, 3, 3'b0, 1, 4, 18, 8'b00000001, 19, 16, False, 18, -1, -1, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1);

    node[20] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 20, 20, False, 21, 24, 23, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    node[21] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 21, 20, False, 20, 22, 25, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    node[22] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 22, 20, False, 21, 23, 26, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    node[23] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 23, 20, False, 22, 27, 20, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    node[24] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 24, 20, False, 20, 25, 27, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    node[25] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 25, 20, False, 24, 21, 26, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    node[26] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 26, 20, True, 25, 22, 27, 18, -1, -1, -1, -1, 26, 26, 26, 18, -1, -1, -1, -1, 2, 2, 2, 1, -1, -1, -1, -1);

    node[27] <- mk_node(3'b0, 1, 3, 3'b11, 2, 4, 26, 8'b00000010, 27, 20, False, 26, 23, 24, -1, -1, -1, -1, -1, 26, 26, 26, -1, -1, -1, -1, -1, 2, 2, 2, -1, -1, -1, -1, -1);

    rule link01 ;
        Bit#(`Flit_width) myflit <- node[1].ma_get_flit0();
        node[0].ma_put_flit0(myflit);
    endrule

    rule link04 ;
        Bit#(`Flit_width) myflit <- node[4].ma_get_flit0();
        node[0].ma_put_flit1(myflit);
    endrule

    rule link10 ;
        Bit#(`Flit_width) myflit <- node[0].ma_get_flit0();
        node[1].ma_put_flit0(myflit);
    endrule

    rule link12 ;
        Bit#(`Flit_width) myflit <- node[2].ma_get_flit0();
        node[1].ma_put_flit1(myflit);
    endrule

    rule link15 ;
        Bit#(`Flit_width) myflit <- node[5].ma_get_flit1();
        node[1].ma_put_flit2(myflit);
    endrule

    rule link21 ;
        Bit#(`Flit_width) myflit <- node[1].ma_get_flit1();
        node[2].ma_put_flit0(myflit);
    endrule

    rule link23 ;
        Bit#(`Flit_width) myflit <- node[3].ma_get_flit0();
        node[2].ma_put_flit1(myflit);
    endrule

    rule link26 ;
        Bit#(`Flit_width) myflit <- node[6].ma_get_flit1();
        node[2].ma_put_flit2(myflit);
    endrule

    rule link32 ;
        Bit#(`Flit_width) myflit <- node[2].ma_get_flit1();
        node[3].ma_put_flit0(myflit);
    endrule

    rule link37 ;
        Bit#(`Flit_width) myflit <- node[7].ma_get_flit1();
        node[3].ma_put_flit1(myflit);
    endrule

    rule link40 ;
        Bit#(`Flit_width) myflit <- node[0].ma_get_flit1();
        node[4].ma_put_flit0(myflit);
    endrule

    rule link45 ;
        Bit#(`Flit_width) myflit <- node[5].ma_get_flit0();
        node[4].ma_put_flit1(myflit);
    endrule

    rule link48 ;
        Bit#(`Flit_width) myflit <- node[8].ma_get_flit0();
        node[4].ma_put_flit2(myflit);
    endrule

    rule link54 ;
        Bit#(`Flit_width) myflit <- node[4].ma_get_flit1();
        node[5].ma_put_flit0(myflit);
    endrule

    rule link51 ;
        Bit#(`Flit_width) myflit <- node[1].ma_get_flit2();
        node[5].ma_put_flit1(myflit);
    endrule

    rule link56 ;
        Bit#(`Flit_width) myflit <- node[6].ma_get_flit0();
        node[5].ma_put_flit2(myflit);
    endrule

    rule link59 ;
        Bit#(`Flit_width) myflit <- node[9].ma_get_flit1();
        node[5].ma_put_flit3(myflit);
    endrule

    rule link65 ;
        Bit#(`Flit_width) myflit <- node[5].ma_get_flit2();
        node[6].ma_put_flit0(myflit);
    endrule

    rule link62 ;
        Bit#(`Flit_width) myflit <- node[2].ma_get_flit2();
        node[6].ma_put_flit1(myflit);
    endrule

    rule link67 ;
        Bit#(`Flit_width) myflit <- node[7].ma_get_flit0();
        node[6].ma_put_flit2(myflit);
    endrule

    rule link610 ;
        Bit#(`Flit_width) myflit <- node[10].ma_get_flit1();
        node[6].ma_put_flit3(myflit);
    endrule

    rule link76 ;
        Bit#(`Flit_width) myflit <- node[6].ma_get_flit2();
        node[7].ma_put_flit0(myflit);
    endrule

    rule link73 ;
        Bit#(`Flit_width) myflit <- node[3].ma_get_flit1();
        node[7].ma_put_flit1(myflit);
    endrule

    rule link711 ;
        Bit#(`Flit_width) myflit <- node[11].ma_get_flit1();
        node[7].ma_put_flit2(myflit);
    endrule

    rule link84 ;
        Bit#(`Flit_width) myflit <- node[4].ma_get_flit2();
        node[8].ma_put_flit0(myflit);
    endrule

    rule link89 ;
        Bit#(`Flit_width) myflit <- node[9].ma_get_flit0();
        node[8].ma_put_flit1(myflit);
    endrule

    rule link812 ;
        Bit#(`Flit_width) myflit <- node[12].ma_get_flit0();
        node[8].ma_put_flit2(myflit);
    endrule

    rule link98 ;
        Bit#(`Flit_width) myflit <- node[8].ma_get_flit1();
        node[9].ma_put_flit0(myflit);
    endrule

    rule link95 ;
        Bit#(`Flit_width) myflit <- node[5].ma_get_flit3();
        node[9].ma_put_flit1(myflit);
    endrule

    rule link910 ;
        Bit#(`Flit_width) myflit <- node[10].ma_get_flit0();
        node[9].ma_put_flit2(myflit);
    endrule

    rule link913 ;
        Bit#(`Flit_width) myflit <- node[13].ma_get_flit1();
        node[9].ma_put_flit3(myflit);
    endrule

    rule link109 ;
        Bit#(`Flit_width) myflit <- node[9].ma_get_flit2();
        node[10].ma_put_flit0(myflit);
    endrule

    rule link106 ;
        Bit#(`Flit_width) myflit <- node[6].ma_get_flit3();
        node[10].ma_put_flit1(myflit);
    endrule

    rule link1011 ;
        Bit#(`Flit_width) myflit <- node[11].ma_get_flit0();
        node[10].ma_put_flit2(myflit);
    endrule

    rule link1014 ;
        Bit#(`Flit_width) myflit <- node[14].ma_get_flit1();
        node[10].ma_put_flit3(myflit);
    endrule

    rule link1018 ;
        Bit#(`Flit_width) myflit <- node[18].ma_get_flit2();
        node[10].ma_put_flit4(myflit);
    endrule

    rule link1110 ;
        Bit#(`Flit_width) myflit <- node[10].ma_get_flit2();
        node[11].ma_put_flit0(myflit);
    endrule

    rule link117 ;
        Bit#(`Flit_width) myflit <- node[7].ma_get_flit2();
        node[11].ma_put_flit1(myflit);
    endrule

    rule link1115 ;
        Bit#(`Flit_width) myflit <- node[15].ma_get_flit1();
        node[11].ma_put_flit2(myflit);
    endrule

    rule link128 ;
        Bit#(`Flit_width) myflit <- node[8].ma_get_flit2();
        node[12].ma_put_flit0(myflit);
    endrule

    rule link1213 ;
        Bit#(`Flit_width) myflit <- node[13].ma_get_flit0();
        node[12].ma_put_flit1(myflit);
    endrule

    rule link1312 ;
        Bit#(`Flit_width) myflit <- node[12].ma_get_flit1();
        node[13].ma_put_flit0(myflit);
    endrule

    rule link139 ;
        Bit#(`Flit_width) myflit <- node[9].ma_get_flit3();
        node[13].ma_put_flit1(myflit);
    endrule

    rule link1314 ;
        Bit#(`Flit_width) myflit <- node[14].ma_get_flit0();
        node[13].ma_put_flit2(myflit);
    endrule

    rule link1413 ;
        Bit#(`Flit_width) myflit <- node[13].ma_get_flit2();
        node[14].ma_put_flit0(myflit);
    endrule

    rule link1410 ;
        Bit#(`Flit_width) myflit <- node[10].ma_get_flit3();
        node[14].ma_put_flit1(myflit);
    endrule

    rule link1415 ;
        Bit#(`Flit_width) myflit <- node[15].ma_get_flit0();
        node[14].ma_put_flit2(myflit);
    endrule

    rule link1514 ;
        Bit#(`Flit_width) myflit <- node[14].ma_get_flit2();
        node[15].ma_put_flit0(myflit);
    endrule

    rule link1511 ;
        Bit#(`Flit_width) myflit <- node[11].ma_get_flit2();
        node[15].ma_put_flit1(myflit);
    endrule

    rule link1617 ;
        Bit#(`Flit_width) myflit <- node[17].ma_get_flit0();
        node[16].ma_put_flit0(myflit);
    endrule

    rule link1716 ;
        Bit#(`Flit_width) myflit <- node[16].ma_get_flit0();
        node[17].ma_put_flit0(myflit);
    endrule

    rule link1718 ;
        Bit#(`Flit_width) myflit <- node[18].ma_get_flit0();
        node[17].ma_put_flit1(myflit);
    endrule

    rule link1817 ;
        Bit#(`Flit_width) myflit <- node[17].ma_get_flit1();
        node[18].ma_put_flit0(myflit);
    endrule

    rule link1819 ;
        Bit#(`Flit_width) myflit <- node[19].ma_get_flit0();
        node[18].ma_put_flit1(myflit);
    endrule

    rule link1810 ;
        Bit#(`Flit_width) myflit <- node[10].ma_get_flit4();
        node[18].ma_put_flit2(myflit);
    endrule

    rule link1826 ;
        Bit#(`Flit_width) myflit <- node[26].ma_get_flit3();
        node[18].ma_put_flit3(myflit);
    endrule

    rule link1918 ;
        Bit#(`Flit_width) myflit <- node[18].ma_get_flit1();
        node[19].ma_put_flit0(myflit);
    endrule

    rule link2021 ;
        Bit#(`Flit_width) myflit <- node[21].ma_get_flit0();
        node[20].ma_put_flit0(myflit);
    endrule

    rule link2024 ;
        Bit#(`Flit_width) myflit <- node[24].ma_get_flit0();
        node[20].ma_put_flit1(myflit);
    endrule

    rule link2023 ;
        Bit#(`Flit_width) myflit <- node[23].ma_get_flit2();
        node[20].ma_put_flit2(myflit);
    endrule

    rule link2120 ;
        Bit#(`Flit_width) myflit <- node[20].ma_get_flit0();
        node[21].ma_put_flit0(myflit);
    endrule

    rule link2122 ;
        Bit#(`Flit_width) myflit <- node[22].ma_get_flit0();
        node[21].ma_put_flit1(myflit);
    endrule

    rule link2125 ;
        Bit#(`Flit_width) myflit <- node[25].ma_get_flit1();
        node[21].ma_put_flit2(myflit);
    endrule

    rule link2221 ;
        Bit#(`Flit_width) myflit <- node[21].ma_get_flit1();
        node[22].ma_put_flit0(myflit);
    endrule

    rule link2223 ;
        Bit#(`Flit_width) myflit <- node[23].ma_get_flit0();
        node[22].ma_put_flit1(myflit);
    endrule

    rule link2226 ;
        Bit#(`Flit_width) myflit <- node[26].ma_get_flit1();
        node[22].ma_put_flit2(myflit);
    endrule

    rule link2322 ;
        Bit#(`Flit_width) myflit <- node[22].ma_get_flit1();
        node[23].ma_put_flit0(myflit);
    endrule

    rule link2327 ;
        Bit#(`Flit_width) myflit <- node[27].ma_get_flit1();
        node[23].ma_put_flit1(myflit);
    endrule

    rule link2320 ;
        Bit#(`Flit_width) myflit <- node[20].ma_get_flit2();
        node[23].ma_put_flit2(myflit);
    endrule

    rule link2420 ;
        Bit#(`Flit_width) myflit <- node[20].ma_get_flit1();
        node[24].ma_put_flit0(myflit);
    endrule

    rule link2425 ;
        Bit#(`Flit_width) myflit <- node[25].ma_get_flit0();
        node[24].ma_put_flit1(myflit);
    endrule

    rule link2427 ;
        Bit#(`Flit_width) myflit <- node[27].ma_get_flit2();
        node[24].ma_put_flit2(myflit);
    endrule

    rule link2524 ;
        Bit#(`Flit_width) myflit <- node[24].ma_get_flit1();
        node[25].ma_put_flit0(myflit);
    endrule

    rule link2521 ;
        Bit#(`Flit_width) myflit <- node[21].ma_get_flit2();
        node[25].ma_put_flit1(myflit);
    endrule

    rule link2526 ;
        Bit#(`Flit_width) myflit <- node[26].ma_get_flit0();
        node[25].ma_put_flit2(myflit);
    endrule

    rule link2625 ;
        Bit#(`Flit_width) myflit <- node[25].ma_get_flit2();
        node[26].ma_put_flit0(myflit);
    endrule

    rule link2622 ;
        Bit#(`Flit_width) myflit <- node[22].ma_get_flit2();
        node[26].ma_put_flit1(myflit);
    endrule

    rule link2627 ;
        Bit#(`Flit_width) myflit <- node[27].ma_get_flit0();
        node[26].ma_put_flit2(myflit);
    endrule

    rule link2618 ;
        Bit#(`Flit_width) myflit <- node[18].ma_get_flit3();
        node[26].ma_put_flit3(myflit);
    endrule

    rule link2726 ;
        Bit#(`Flit_width) myflit <- node[26].ma_get_flit2();
        node[27].ma_put_flit0(myflit);
    endrule

    rule link2723 ;
        Bit#(`Flit_width) myflit <- node[23].ma_get_flit1();
        node[27].ma_put_flit1(myflit);
    endrule

    rule link2724 ;
        Bit#(`Flit_width) myflit <- node[24].ma_get_flit2();
        node[27].ma_put_flit2(myflit);
    endrule

    method Action top_gen_flit(int src, int dest, Bit#(8) dest_tile, int data); 
        node[src].gen_flit(dest, dest_tile, data); 
    endmethod 

endmodule
