//`include "parameter.bsv"

typedef struct {
    Bit#(32) out_i;
    Bit#(8) vc;
} Out deriving(Bits, Eq);

function int chain(int currNode, int destNode);
    if (currNode > destNode)
      return currNode - 1;
    else
      return currNode + 1;
endfunction

function int min_dist(int id1, int id2, int n);
    if(abs(id1-id2) <= n>>1)
        return id1-id2;
    else begin
        if(id1-id2 <= 0)
            return  n+id1-id2;
        else 
            return id1-id2-n;
    end
endfunction

function Out ring(int currNode, int destNode, Bit#(8) prevVc, int n);
    Out out;
    out.vc = prevVc;
    //if we've already reached destination node return -1
    // if (currNode == destNode) begin
    //     if(destNode == n>>1 + 1)
    //         out.vc = 1;
    //         out.out_i = -1;
    //     return out;
    // end

    if (min_dist(currNode,destNode, n) > 0) begin
      //traverse anticlockwise
        if(currNode - 1 == n>>1) begin  //crosses dateline
            out.vc = 1;
            //$display("change vc to 1");
        end

        if(currNode==0)
            out.out_i = pack(n-1);

        else
            out.out_i = pack(currNode - 1);

        return out;
    end

    else begin
        //traverse clockwise
        if(currNode + 1 == n>>1 + 1) begin //crosses dateline
            out.vc = 1;
            //$display("change vc to 1");
        end

        if(currNode==n-1)
            out.out_i = 0;
        else
            out.out_i = pack(currNode + 1);
        
        return out;
    end
endfunction

function int mesh(int currNode, int destNode, int n);
    int si = currNode / n;
    int sj = currNode % n; // very expensive, there will be easier way to do this
    int di = destNode / n;
    int dj = destNode % n; // very expensive, there will be easier way to do this

    if ((dj>=sj) && (di>=si))
      if ((dj-sj)> (di-si))
        return n*si+sj+1;
      else
        return n*si+sj+n;
    else if ((dj<=sj) && (di>=si))
      if ((sj-dj)> (di-si))
        return n*si+sj-1;
      else
        return n*si+sj+n;
    else if ((dj>=sj) && (di<=si))
      if ((dj-sj)> (si-di))
        return n*si+sj+1;
      else
        return n*si+sj-n;
    else
      if ((sj-dj)> (si-di))
        return n*si+sj-1;
      else
        return n*si+sj-n;
endfunction

function Bool direction_ft(int id1, int id2, int n);
    // direction = True means clockwise
    Bool dir = False;
    //print(min_dist(id1, id2, N))
    if (min_dist(id1, id2, n)<0)
        dir = True;
    else
        dir = False;
    return dir;
endfunction


function Out ft(int currNode, int destNode, int n, int m, Bit#(8) prevVc);
    //if we've already reached destination node return -1
    int si = currNode / n;
    int sj = currNode % n; 
    int di = destNode / n;
    int dj = destNode % n;

    Out out;
    out.vc = prevVc;
    Bool direction = False;

    // if currNode == destNode:
    //     return -1, 0

    //X distance
    if (dj != sj) begin  //traverse columns
        direction = direction_ft(sj, dj, m);
        if(direction) 
            sj = sj + 1;
        else
            sj = sj - 1;
        sj = sj%m;
        out.out_i = pack(n*si + sj); // Add +1 or -1
    end

    else begin  //traverse ring
        direction = direction_ft(si, di, n);
        if(direction) 
            si = si + 1;
        else
            si = si - 1;
        si = si%n;
        out.out_i = pack(n*si + sj);

        if(!direction &&& si - 1 == n>>1)   //crosses dateline
            out.vc = 1;

        else if(direction &&& si == n>>1 + 1) //crosses dateline
            out.vc = 1;
    end
    return out;
endfunction

function Out hypercube(int currNode, int destNode);
        Bit#(3) d_bit = truncate(pack(destNode));
        Bit#(3) s_bit = truncate(pack(currNode));
        Bit#(3) next_node = s_bit;
        Out out;

        //find next node using MSB to LSB routing
        if (d_bit[0] != s_bit[0])
            next_node[0] = d_bit[0];
        else if (d_bit[1] != s_bit[1])
            next_node[1] = d_bit[1];
        else
            next_node[2] = d_bit[2];
        
        out.out_i = zeroExtend(next_node);
        out.vc = zeroExtend(d_bit);

        return out;
endfunction

function Bit#(32) butterfly(int currNode, int destNode, int n);
        int diff = (currNode^ destNode);
        int diff_lsb = diff;//(currNode^ destNode);
        Bit#(32) i = 0;
        Bit#(32) j = 0;
        int one = 1;

        Bit#(32) msb_diff_loc = 0;
        Bit#(32) lsb_diff_loc = 0;
        Bit#(32) next_node = 0;
        if (diff>0) begin
            while (diff>0) begin
                diff = diff>>1;
                i = i+1;
            end
            msb_diff_loc = 1<<i;
        end
        if (pack(diff_lsb&one) == 0) begin
            while (pack(diff_lsb&one) == 0) begin
                j = j+1;
            end
            lsb_diff_loc = 1<<j;
        end

        if (destNode < n) begin
            next_node = (pack(currNode) ^ lsb_diff_loc);
        end
        else begin
            next_node = (pack(currNode) ^ msb_diff_loc);
        end
        
        return next_node;
endfunction



interface Ifc_Router;
    method ActionValue#(Out) ma_route(Bit#(3) topology, int m, int n, int nodeid, int currDest, Bit#(8) prevVc);
endinterface


(*synthesize*)
module mk_router(Ifc_Router);

    method ActionValue#(Out) ma_route(Bit#(3) topology, int m, int n, int nodeid, int currDest, Bit#(8) prevVc);
        Out out;
        out.out_i = 0;
        out.vc = prevVc;
        if(topology == 3'b000)
            out.out_i = pack(chain(nodeid, currDest));
        else if(topology == 3'b001)
            out = ring(nodeid, currDest, prevVc, n);
        else if(topology == 3'b010)
            out.out_i = pack(mesh(nodeid, currDest, n));
        else if(topology == 3'b011)
            out = ft(nodeid, currDest, n, m, prevVc);
        else if(topology == 3'b100)
            out = hypercube(nodeid, currDest);
        //else if(topology == 3'b101)
        //    out.out_i = butterfly(nodeid, currDest, n);
        return out;
    endmethod

endmodule

