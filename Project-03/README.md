# PROJECT 03

This folder contains the files for project03.

1. [project-Cs6230.pdf](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/project-Cs6230.pdf)  : Contains the problem statement as given to us.
2. [p3topgen.py](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/p3topgen.py)  : Python code to generate network.bsv, the top bsv file with the topology using the L1Topology.txt and L2Topology.txt files as input. The script to generate the bsv file is built upon the python code of the previous 2 projects.
3. [network.bsv](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/network.bsv)  : bsv module created by the python script. Instantiates all network nodes and links them together. The links are modelled as rules to enqueue and dequeue from corresponding nodes. Any 2 neighbouring nodes have 2 links in between them.
Contains a method to call the generate flit method of any required node.
3. [node.bsv](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/node.bsv)  : bsv module for a node in the network. Contains methods to get flits, put flits and generate a flit. Router modules are called inside rules, and the arbiters are modelled as rules. Various FIFOs for inputs and outputs are 
4. [router.bsv](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/router.bsv)  : bsv module for the routers inside every node. Routes the flit according to the topology and destination. Modelled similar to project 2 routings.
5. [parameter.bsv](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/parameter.bsv)  : file with parameters.
6. [tb_noc.bsv](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/tb_noc.bsv)  : bsv testbench to test the network on chip. 
7. [run.sh](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-03/run.sh)  : script to execute the test.

## Usage Syntax

Alter the L1Topology.txt and L2Topology.txt as needed. 

Uncomment the python command in run.sh, use any version of python >=3.0.

Run the following commands:

- $chmod +x run.sh
- ./run.sh



## Input format:
The L1 file will contain only 1 line, which specifies the higher order topology of the network. This line will be the form:
`X, n, m`, where the notation is the same as given in the [problem statement](https://gitlab.com/parsidd/noc_project/-/blob/main/Project-01/project1.txt) of Project01. The L2 file describes each "tile"  of the network as we call it, which itself is a network of nodes. Each line in the L2 file will be of the same format as that of the single line in the L1 file. **Note** that we require each line to follow the correct format and must have all three parameters; in case of a malformatted file, the code will send a message on the standard output terminal, indicating the error faced or assumption made.

## Structure of the Network:

![Network architecture](Network.png?raw=true)

The head nodes form the L1 topology, each tile is the part of L2 topology. A flit has to travel a maximum of 3 routes : 
1. In its origin tile in the L2 layer.
2. In the L1 layer.
3. In its destination tile in the L2 layer.

## Structure of a Node:


![Node architecture](node.png?raw=true)

- IBx refers to Input Buffer modelled as a FIFO.
- OBx refers to Output Buffer modelled as a FIFO.
- RBx refers to Router Buffer modelled as a FIFO of depth = VC_size.
- Rx refers to Router.
- Ax refers to Arbiter.
- Additionally, each node has flit generating module modelled as a method.

## Structure of a Flit:

A flit contains the flit data and other information required to route it in the network. Our flit is of size 24B, of which 32b is the data.

Format of the flit data - 8'b previous vc, 8'b start tile id, 8'b previous tile id, 8'b destination tile id, 32'b start address, 32'b previous address, 32'b current destination, 32'b final destination, 32'b data

Total Flit_width = 192 bits
            


## Authors:
**Team Flip-flops:**
| Name | Roll number|
| ------ | ------ |
| Mansi Choudhary | EE17B053 |
| Vighnesh Natarajan | EE17B119 |
| Parth S Shah | EE17B059 |

